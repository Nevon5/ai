#ifndef TERRANSTRATEGYPLANNER_H
#define TERRANSTRATEGYPLANNER_H

#include "StrategyPlanner.h"

using namespace std;


class TerranStrategyPlanner : public StrategyPlanner
{
private:
	void counter( std::vector<Squad*>& p_Squad );
public:
	TerranStrategyPlanner( vector<BuildplanEntry>* p_Calcbuilder, int* p_noOfWorkers  );
	~TerranStrategyPlanner(void);

	bool OpeningStrategy(int p_Supply, std::vector<Squad*>& p_Squad);
	bool EarlyStrategy(int p_Supply, std::vector<Squad*>& p_Squad);
	bool MiddleStrategy(int p_Supply, std::vector<Squad*>& p_Squad);
	bool LateStrategy(int p_Supply, std::vector<Squad*>& p_Squad);
	void AllDoneStrategy(int p_Supply, std::vector<Squad*>& p_Squad);
};

#endif