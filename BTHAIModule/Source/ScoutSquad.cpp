#include "ScoutSquad.h"
#include "UnitAgent.h"
#include "AgentManager.h"
#include "ExplorationManager.h"
#include <math.h>
#define PI 3.14159265


ScoutSquad::ScoutSquad(int mId, string mName, int mPriority, int p_Type)
	: Squad ( mId, SCOUT, mName, mPriority )
{
	this->moveType = p_Type;
	goal = getNextStartLocation();
	currentState = STATE_NOT_SET;
	m_ScoutState = Idle;

}


bool ScoutSquad::isActive()
{
	return active;
}

void ScoutSquad::defend(TilePosition mGoal)
{

}

void ScoutSquad::attack(TilePosition mGoal)
{

}

void ScoutSquad::assist(TilePosition mGoal)
{

}

void ScoutSquad::checkAlive(void)
{
	if(!agents.at(0)->isAlive())
		agents.erase(agents.begin());
}
float angle = 0;
void ScoutSquad::computeActions()
{
	if(agents.size() < 0)
		return;

	if(!active)
	{
		if(isFull())
			active = true;
		else
			return;
	}

	if(m_ScoutState == Idle)
	{
		setGoal(getNextStartLocation());
		m_ScoutState = Moving;
	}

	else if(m_ScoutState == Moving)
	{
		if(agents.at(0)->getUnit()->getPosition().getApproxDistance(Position(goal)) <= 60)
			m_ScoutState = Idle;


		if(ExplorationManager::getInstance()->buildingsSpotted())
		{
			TilePosition enemyBase = ExplorationManager::getInstance()->getEnemyBaseLocation( );
			if(enemyBase.x() < 0 && enemyBase.y() < 0)
				ExplorationManager::getInstance()->setEnemyBaseLocation( goal );

			m_ScoutState = Scouting;
		}
	}

	else if(m_ScoutState == Scouting)
	{

		if(agents.at(0)->isUnderAttack() || enemyDefenseInRange() ||
			agents.at(0)->getUnit()->getShields() < agents.at(0)->getUnit()->getType().maxShields() * 0.6)
		{
			setGoal(Broodwar->self()->getStartLocation());
			m_ScoutState = Escaping;	
		}
		else
		{
			if(goal == Broodwar->self()->getStartLocation())
				setGoal(ExplorationManager::getInstance()->getEnemyBaseLocation( ));
			
			if(agents.at(0)->getUnit()->getPosition().getApproxDistance(Position(goal)) <= 120)
			{

				float X = 0;
				float Y = 0;
				TilePosition enemyBase = ExplorationManager::getInstance()->getEnemyBaseLocation( );
				X = enemyBase.x() + (cos(angle) * 6.8f) + ((float(rand()) / float(RAND_MAX)) * (4 + 4)) - 4;
				Y = enemyBase.y() + (sin(angle) * 6.8f) + ((float(rand()) / float(RAND_MAX)) * (4 + 4)) - 4;


				setGoal(TilePosition(X,Y));
				angle += 0.5;

				if(angle >= 2*PI)
					angle = 0;

			}

		}
	}

	else if(m_ScoutState == Escaping)
	{
		if(!agents.at(0)->isUnderAttack() && !agents.at(0)->enemyUnitsVisible())
		{
			//Broodwar->sendText("I'm safe!");

			setMemberGoals(TilePosition(getCenter()));

			if(agents.at(0)->getUnit()->getShields() == agents.at(0)->getUnitType().maxShields())
				m_ScoutState = Scouting;

		}
	}

	agents.at(0)->getUnit()->move(Position(goal));
}

bool ScoutSquad::enemyDefenseInRange(void)
{
	for(std::set<Unit*>::const_iterator i = Broodwar->enemy()->getUnits().begin(); i != Broodwar->enemy()->getUnits().end(); i++)
	{
		if(agents.at(0)->getUnit()->getPosition().getApproxDistance( (*i)->getPosition() ) 
			<= agents.at(0)->getUnitType().seekRange() )
		{
			if(type == GROUND)
			{
				if( (*i)->getType().getID() == UnitTypes::Protoss_Photon_Cannon.getID()
					|| (*i)->getType().getID() == UnitTypes::Zerg_Sunken_Colony.getID()
					|| (*i)->getType().getID() == UnitTypes::Terran_Bunker.getID() )
					return true;
			}
			else if (type == AIR)
			{
				if( (*i)->getType().getID() == UnitTypes::Protoss_Photon_Cannon.getID() 
					|| (*i)->getType().getID() == UnitTypes::Zerg_Spore_Colony.getID() 
					|| (*i)->getType().getID() == UnitTypes::Terran_Bunker.getID() 
					|| (*i)->getType().getID() == UnitTypes::Terran_Missile_Turret.getID() )
					return true;
			}

		}
	}

	return false;
}

bool ScoutSquad::setGoal(TilePosition p_goal)
{
	goal = p_goal;
	setMemberGoals(goal);

	return true;
}

void ScoutSquad::clearGoal()
{
	goal = TilePosition(-1,-1);
	setMemberGoals(goal);
}

TilePosition ScoutSquad::getGoal()
{
	return goal;
}

bool ScoutSquad::hasGoal()
{
	if (goal.x() < 0 || goal.y() < 0)
	{
		return false;
	}
	return true;
}
