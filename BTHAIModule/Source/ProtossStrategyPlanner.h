#ifndef PROTOSSSTRATEGYPLANNER_H
#define PROTOSSSTRATEGYPLANNER_H

#include "StrategyPlanner.h"

using namespace BWAPI;
using namespace std;


class ProtossStrategyPlanner : public StrategyPlanner
{
private:
	void counter( std::vector<Squad*>& p_Squad );
public:
	ProtossStrategyPlanner( vector<BuildplanEntry>* p_Calcbuilder, int* p_noOfWorkers );
	~ProtossStrategyPlanner(void);

	bool OpeningStrategy(int p_Supply, std::vector<Squad*>& p_Squad);
	bool EarlyStrategy(int p_Supply, std::vector<Squad*>& p_Squad);
	bool MiddleStrategy(int p_Supply, std::vector<Squad*>& p_Squad);
	bool LateStrategy(int p_Supply, std::vector<Squad*>& p_Squad);
	void AllDoneStrategy(int p_Supply, std::vector<Squad*>& p_Squad);
};

#endif