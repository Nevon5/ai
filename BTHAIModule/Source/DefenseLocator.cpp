#include "DefenseLocator.h"
#include "AgentManager.h"

DefenseLocator* DefenseLocator::instance = NULL;

DefenseLocator::DefenseLocator()
{
	//Benzene
	data.push_back(DefensePos(7,96,7,96,22,79,"af618ea3ed8a8926ca7b17619eebcb9126f0d8b1"));
	data.push_back(DefensePos(7,96,13,68,10,56,"af618ea3ed8a8926ca7b17619eebcb9126f0d8b1"));
	data.push_back(DefensePos(7,96,13,11,21,45,"af618ea3ed8a8926ca7b17619eebcb9126f0d8b1"));

	data.push_back(DefensePos(117,13,117,13,105,30,"af618ea3ed8a8926ca7b17619eebcb9126f0d8b1"));
	data.push_back(DefensePos(117,13,112,41,116,52,"af618ea3ed8a8926ca7b17619eebcb9126f0d8b1"));
	data.push_back(DefensePos(117,13,112,70,102,61,"af618ea3ed8a8926ca7b17619eebcb9126f0d8b1"));

	//Fading Realm
	data.push_back(DefensePos(25,18,25,18,85,20,"d05dbc04919ce95859eb4b42fcd04837b4ea5df3"));
	data.push_back(DefensePos(25,18,114,7,91,37,"d05dbc04919ce95859eb4b42fcd04837b4ea5df3"));
	data.push_back(DefensePos(25,18,116,33,91,37,"d05dbc04919ce95859eb4b42fcd04837b4ea5df3"));
	
	data.push_back(DefensePos(99,74,99,74,45,68,"d05dbc04919ce95859eb4b42fcd04837b4ea5df3"));
	data.push_back(DefensePos(99,74,7,60,46,52,"d05dbc04919ce95859eb4b42fcd04837b4ea5df3"));
	data.push_back(DefensePos(99,74,63,43,72,42,"d05dbc04919ce95859eb4b42fcd04837b4ea5df3"));

	//Andromeda

		// UH
	data.push_back(DefensePos(117,7,117,7,114,31,"1e983eb6bcfa02ef7d75bd572cb59ad3aab49285"));
	data.push_back(DefensePos(117,7,108,36,114,31,"1e983eb6bcfa02ef7d75bd572cb59ad3aab49285"));
	data.push_back(DefensePos(117,7,103,22,96,31,"1e983eb6bcfa02ef7d75bd572cb59ad3aab49285"));

		// NV
	data.push_back(DefensePos(7,118,7,118,16,95,"1e983eb6bcfa02ef7d75bd572cb59ad3aab49285"));
	data.push_back(DefensePos(7,118,14,89,16,95,"1e983eb6bcfa02ef7d75bd572cb59ad3aab49285"));
	data.push_back(DefensePos(7,118,20,104,29,96,"1e983eb6bcfa02ef7d75bd572cb59ad3aab49285"));

		// NR
	data.push_back(DefensePos(117,119,117,119,113,94,"1e983eb6bcfa02ef7d75bd572cb59ad3aab49285"));
	data.push_back(DefensePos(117,119,117,119,113,94,"1e983eb6bcfa02ef7d75bd572cb59ad3aab49285"));
	data.push_back(DefensePos(117,119,103,105,98,96,"1e983eb6bcfa02ef7d75bd572cb59ad3aab49285"));

		// UV
	data.push_back(DefensePos(7,6,7,6,12,31,"1e983eb6bcfa02ef7d75bd572cb59ad3aab49285"));
	data.push_back(DefensePos(7,6,7,6,14,36,"1e983eb6bcfa02ef7d75bd572cb59ad3aab49285"));
	data.push_back(DefensePos(7,6,7,6,14,36,"1e983eb6bcfa02ef7d75bd572cb59ad3aab49285"));
	data.push_back(DefensePos(7,6,20,22,30,32,"1e983eb6bcfa02ef7d75bd572cb59ad3aab49285"));


	//Fortress

		//V
	data.push_back(DefensePos(13,74,13,74,20,50,"83320e505f35c65324e93510ce2eafbaa71c9aa1"));
	data.push_back(DefensePos(13,74,9,49,20,50,"83320e505f35c65324e93510ce2eafbaa71c9aa1"));
	data.push_back(DefensePos(13,74,34,72,20,50,"83320e505f35c65324e93510ce2eafbaa71c9aa1"));


		//U
	data.push_back(DefensePos(49,7,49,7,76,16,"83320e505f35c65324e93510ce2eafbaa71c9aa1"));
	data.push_back(DefensePos(49,7,81,7,76,16,"83320e505f35c65324e93510ce2eafbaa71c9aa1"));
	data.push_back(DefensePos(49,7,53,34,76,16,"83320e505f35c65324e93510ce2eafbaa71c9aa1"));

		//H
	data.push_back(DefensePos(117,54,117,54,106,77,"83320e505f35c65324e93510ce2eafbaa71c9aa1"));
	data.push_back(DefensePos(117,54,115,78,106,77,"83320e505f35c65324e93510ce2eafbaa71c9aa1"));
	data.push_back(DefensePos(117,54,90,54,106,77,"83320e505f35c65324e93510ce2eafbaa71c9aa1"));

		//N
	data.push_back(DefensePos(77,119,77,119,47,113,"83320e505f35c65324e93510ce2eafbaa71c9aa1"));
	data.push_back(DefensePos(77,119,43,118,47,113,"83320e505f35c65324e93510ce2eafbaa71c9aa1"));
	data.push_back(DefensePos(77,119,71,92,47,113,"83320e505f35c65324e93510ce2eafbaa71c9aa1"));
}

DefenseLocator::~DefenseLocator()
{
	instance = NULL;
}

DefenseLocator* DefenseLocator::getInstance()
{
	if (instance == NULL)
	{
		instance = new DefenseLocator();
	}
	return instance;
}


TilePosition DefenseLocator::getBaseDefensePos(string mMaphash)
{
	TilePosition pos = TilePosition(-1,-1);
	int count = 0;
	BaseAgent* lastNexus = NULL;

	vector<BaseAgent*> agents = AgentManager::getInstance()->getAgents();

	for(int i = 0; i < agents.size(); i++)
	{
		if( agents.at(i)->getUnitType().isResourceDepot() && agents.at(i)->isAlive() )
			lastNexus = agents.at(i);
	}

	if(lastNexus != NULL)
	{

		for(int i = 0; i < (int)data.size(); i++)
		{
			if( data[i].maphash == mMaphash)
			{
				TilePosition dBase = lastNexus->getUnit()->getTilePosition();
				TilePosition hBase = Broodwar->self()->getStartLocation();

				if (data[i].matches(hBase,dBase))
				{
					return data[i].pos;
				}
			}
		}

	}

	return TilePosition(-1,-1);
}
