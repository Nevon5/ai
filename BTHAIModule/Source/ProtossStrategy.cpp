﻿#include "ProtossStrategy.h"
#include "BuildplanEntry.h"
#include "ExplorationSquad.h"
#include "ExplorationManager.h"
#include "AgentManager.h"
#include "BuildingPlacer.h"
#include "Upgrader.h"
#include "Constructor.h"
#include "ArbiterAgent.h"

ProtossStrategy::ProtossStrategy()
{	
	ExplorationManager::getInstance()->Initialize();

	squadID = 0;
	noWorkers = 27;

	noWorkersPerRefinery = 3;
	if ( Broodwar->enemy()->getRace().getName( ) == Races::Protoss.getName( ))
	{
		m_StratPlanner = new ProtossStrategyPlanner( &CalcBuildPlan, &noWorkers );
	}
	else if ( Broodwar->enemy()->getRace().getID( ) == Races::Terran.getID( ) )
	{
		m_StratPlanner = new TerranStrategyPlanner( &CalcBuildPlan, &noWorkers );
	}
	else if ( Broodwar->enemy()->getRace().getID( ) == Races::Zerg.getID( ) )
	{
		m_StratPlanner = new ZergStrategyPlanner( &CalcBuildPlan, &noWorkers );
	}
	else
	{
		// This should never occur.
	}

	//Broodwar->sendText("show me the money");
	//Broodwar->sendText("operation cwal");
	

	zergRush = false;
	inBattle = false;
	underAttack = false;
	haveGasPro = false;
	winningBattle = true;
	BuildBuildings = true;
	haveSingularityCharge = false;
	//OpeningCheck = true;
	m_GameTimeStage = opening;

	lastbuildpylon = 9;


}
ProtossStrategy::~ProtossStrategy()
{
	for (int i = 0; i < (int)squads.size(); i++)
	{
		delete squads.at(i);
	}
	instance = NULL;

	if(m_StratPlanner)
	{
		m_StratPlanner->~StrategyPlanner();
		delete(m_StratPlanner);
	}
}

void ProtossStrategy::computeActions()
{
	if( Broodwar->getFrameCount() % 24 == 0)
		return;


	computeActionsBase();
	AICommander();

	ExplorationManager::getInstance()->computeActions();

	for(std::set<Unit*>::const_iterator i = Broodwar->self()->getUnits().begin(); i != Broodwar->self()->getUnits().end(); i++)
	{
		if( !(*i)->getType().isBuilding() )
			continue;

		if( (*i)->getHitPoints() <= 50 && (*i)->isUnderAttack())
		{
			if( (*i)->isBeingConstructed() )
				(*i)->cancelConstruction();

			else if( (*i)->isTraining() )
				(*i)->cancelTrain();

			else if( (*i)->isResearching() )
				(*i)->cancelResearch();
		}

	}
}
bool fu = true;
void ProtossStrategy::CalcWhatToBuild( )
{

	ExplorationManager::getInstance()->getNoDetectors(Enemy);

	if ( cSupply > 60 && fu)
	{
		Squad* a = new Squad( 100, Squad::OFFENSIVE, "teleport", 1 );
		a->addSetup(UnitTypes::Protoss_Arbiter, 1 );
		a->setBuildup(true);
		a->setRequired(true);
		squads.push_back( a );
		m_StratPlanner->teleportdude( );
		fu = false;
	}
	if ( !fu )
	{
		for ( int i = 0; i < squads.size( ); i++)
		{
			if (squads[i]->getID( ) == 100)
			{
				if ( squads[i]->isFull( ) )
				{
					((ArbiterAgent*)(squads[i]->getMembers( )[0]))->teleport
						( m_StratPlanner->getCounter( )->getCenter( ),TilePosition( 20, 100 ) );
					squads[i]->setGoal( TilePosition( 20, 100 ) );
				
				}
			}
		}
	}
	switch (m_GameTimeStage)
	{
	case opening:
		if(m_StratPlanner->OpeningStrategy(cSupply, squads ))
			m_GameTimeStage = early;
		break;
	case early:
		if(m_StratPlanner->EarlyStrategy(cSupply, squads ))
			m_GameTimeStage = middle;
		break;
	case middle:
		if(m_StratPlanner->MiddleStrategy(cSupply, squads ))
			m_GameTimeStage = late;
		break;
	case late:
		if(m_StratPlanner->LateStrategy(cSupply, squads ))
			m_GameTimeStage = allDone;
		break;
	case allDone:
		m_StratPlanner->AllDoneStrategy(cSupply, squads );
		break;
	default:
		break;
	}

}

void ProtossStrategy::AICommander( )
{

	cSupply = Broodwar->self()->supplyUsed() / 2;
	tSupply = Broodwar->self()->supplyTotal() / 2;
	min = Broodwar->self()->minerals();
	gas = Broodwar->self()->gas();

	CalcWhatToBuild( );

	// fix chokepoint

	if ( !underAttack && BuildBuildings )
	{
		if ( buildplan.size( ) == 0 && CalcBuildPlan.size( ) != 0)
		{
			if ( Broodwar->canMake( NULL, CalcBuildPlan[0].unittype) ||  CalcBuildPlan[0].supply != 0)
			{
				if ( !Constructor::getInstance()->isBuilding( ) )
				{
					if ( CalcBuildPlan[0].supply == 0 )
					{
						CalcBuildPlan[0].supply = cSupply;
					}
						buildplan.push_back( CalcBuildPlan[0] );
						CalcBuildPlan.erase( CalcBuildPlan.begin( ) );
				}
			}
		}
	}
}

void ProtossStrategy::addUnitToBuildPlan( BuildplanEntry p_BuildPlanEntry )
{
	CalcBuildPlan.push_back(p_BuildPlanEntry);
}