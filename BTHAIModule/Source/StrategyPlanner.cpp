﻿#include "StrategyPlanner.h"

StrategyPlanner::StrategyPlanner( vector<BuildplanEntry>* p_Calcbuilder, int* p_noOfWorkers )
{
	m_CalcBuildPlan = p_Calcbuilder;
	squadID = 0;
	m_NoOfWorkers = p_noOfWorkers;
	forge = false;
	Shieldbattery = false;
	cyberneticsCore = false;
	roboticsFacility = false;
	roboticsSupportBay = false;
	observatory = false;
	stargate = false;
	fleetBeacon = false;
	arbiterTribunal = false;
	citadelOfAdun = false;
	templarArchives = false;
	haveStorm = false;
	ta = false;
	forceAttack = true;
}

void StrategyPlanner::teleportdude( )
{
	buildarbiterTribunal();
	m_CalcBuildPlan->push_back(BuildplanEntry( TechTypes::Recall, 0 ) ) ; 
	m_CalcBuildPlan->push_back(BuildplanEntry( TechTypes::Stasis_Field, 0 ) ) ; 
}
StrategyPlanner::~StrategyPlanner( void )
{
	if(m_CalcBuildPlan)
	{
		m_CalcBuildPlan->clear();
		delete m_CalcBuildPlan;
	}

	if(m_Squad)
	{
		m_Squad->~Squad();
		delete m_Squad;
	}
}

int StrategyPlanner::getNoUnit( vector<UnitSetup>& a, UnitType unit )
{
	int noUnit = 0;
	for ( int i = 0; i < a.size( ); i++ )
	{
		if ( a[i].type == unit)
		{
			noUnit += a[i].no;
		}
	}
	return noUnit;
}

void StrategyPlanner::buildforge(int value )
{
	if ( !forge )
	{
		m_CalcBuildPlan->push_back(BuildplanEntry(UnitTypes::Protoss_Forge, value));
		forge = true;
	}
}
void StrategyPlanner::buildShieldbattery()
{
	if ( !Shieldbattery )
	{
		m_CalcBuildPlan->push_back(BuildplanEntry(UnitTypes::Protoss_Shield_Battery, 0));
		Shieldbattery = true;
	}
}
void StrategyPlanner::buildcyberneticsCore()
{
	if ( !cyberneticsCore )
	{
		m_CalcBuildPlan->push_back(BuildplanEntry(UnitTypes::Protoss_Cybernetics_Core, 0));
		cyberneticsCore = true;
	}
}
void StrategyPlanner::buildroboticsFacility()
{
	buildcyberneticsCore( );

	if ( !roboticsFacility )
	{
		m_CalcBuildPlan->push_back(BuildplanEntry(UnitTypes::Protoss_Robotics_Facility, 0));
		roboticsFacility = true;
	}
}
void StrategyPlanner::buildroboticsSupportBay()
{
	buildroboticsFacility( );

	if ( !roboticsSupportBay )
	{
		m_CalcBuildPlan->push_back(BuildplanEntry(UnitTypes::Protoss_Robotics_Support_Bay, 0));
		roboticsSupportBay = true;
	}
}
void StrategyPlanner::buildobservatory()
{
	buildroboticsFacility( );

	if ( !observatory )
	{
		m_CalcBuildPlan->push_back(BuildplanEntry(UnitTypes::Protoss_Observatory, 0));
		observatory = true;
	}
}
void StrategyPlanner::buildstargate()
{
	buildcyberneticsCore( );

	if ( !stargate )
	{
		m_CalcBuildPlan->push_back(BuildplanEntry(UnitTypes::Protoss_Stargate, 0));
		stargate = true;
	}
}
void StrategyPlanner::buildfleetBeacon()
{
	buildstargate( );

	if ( !fleetBeacon )
	{
		m_CalcBuildPlan->push_back(BuildplanEntry(UnitTypes::Protoss_Fleet_Beacon, 0));
		fleetBeacon = true;
	}
}
void StrategyPlanner::buildarbiterTribunal()
{
	buildstargate( );
	buildtemplarArchives( );
	if ( !arbiterTribunal )
	{
		m_CalcBuildPlan->push_back(BuildplanEntry(UnitTypes::Protoss_Arbiter_Tribunal, 0));
		arbiterTribunal = true;
	}
}
void StrategyPlanner::buildcitadelOfAdun()
{
	buildcyberneticsCore( );

	if ( !citadelOfAdun )
	{
		m_CalcBuildPlan->push_back(BuildplanEntry(UnitTypes::Protoss_Citadel_of_Adun, 0));
		citadelOfAdun = true;
	}
}
void StrategyPlanner::buildtemplarArchives()
{
	buildcitadelOfAdun( );

	if ( !templarArchives )
	{
		m_CalcBuildPlan->push_back(BuildplanEntry(UnitTypes::Protoss_Templar_Archives, 0));
		templarArchives = true;
	}
}

void StrategyPlanner::PhotonCannons( void )
{

	// IF cannons < 3
	if( noCannons < ExplorationManager::getInstance()->getNoUnitsByType( Self, UnitTypes::Protoss_Nexus ) * 3 + ( 1 *  ExplorationManager::getInstance()->getNoUnitsByType( Self, UnitTypes::Protoss_Nexus )-1 ) )
	{
		// Om vi SKA bygga choke pylon
		if( BuildingPlacer::getInstance()->placeChokePylon == 1 )
		{

			m_CalcBuildPlan->insert(m_CalcBuildPlan->begin(), BuildplanEntry( UnitTypes::Protoss_Pylon, 0 ) );

			BuildingPlacer::getInstance()->lastChokePylon = NULL;
			BuildingPlacer::getInstance()->placeChokePylon = 2;

		}

		//Om vi inte ska bygga choke pylon
		else if (BuildingPlacer::getInstance()->placeChokePylon == 0)
		{
			//Vi har byggt en choke pylon
			if ( BuildingPlacer::getInstance()->lastChokePylon != NULL)
			{
				//Om den är färdig, lägg till 3 photon cannons
				if( BuildingPlacer::getInstance()->lastChokePylon->isCompleted() )
				{
					for(int i = 0; i < 3 + ( 1 *  ExplorationManager::getInstance()->getNoUnitsByType( Self, UnitTypes::Protoss_Nexus )-1 ); i++)
					{
						noCannons++;
						m_CalcBuildPlan->insert(m_CalcBuildPlan->begin(), BuildplanEntry ( UnitTypes::Protoss_Photon_Cannon, 0) );
					}
				}
			}
		}

	}
}