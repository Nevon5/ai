#ifndef STRATEGYPLANNER_H
#define STRATEGYPLANNER_H


#include "BuildplanEntry.h"
#include "BuildingPlacer.h"
#include "WorkerAgent.h"
#include "AgentManager.h"
#include "ScoutSquad.h"
#include "RushSquad.h"
#include "ExplorationManager.h"
//#include "BaseAgent.h"
//#include "UnitSetup.h"


using namespace BWAPI;
using namespace std;


class StrategyPlanner
{
public:
	StrategyPlanner(vector<BuildplanEntry>* p_Calcbuilder, int* p_noOfWorkers );
	~StrategyPlanner(void);

	virtual bool OpeningStrategy(int p_Supply, std::vector<Squad*>& p_Squad) = 0;
	virtual bool EarlyStrategy(int p_Supply, std::vector<Squad*>& p_Squad) = 0;
	virtual bool MiddleStrategy(int p_Supply, std::vector<Squad*>& p_Squad) = 0;
	virtual bool LateStrategy(int p_Supply, std::vector<Squad*>& p_Squad) = 0;
	virtual void AllDoneStrategy(int p_Supply, std::vector<Squad*>& p_Squad) = 0;

	Squad* getCounter( ) { return counterSquad; }
	void teleportdude( );
protected:
	bool canbuildObs;

	bool forge;
	bool Shieldbattery;
	bool cyberneticsCore;
	bool roboticsFacility;
	bool roboticsSupportBay;
	bool observatory;
	bool stargate;
	bool fleetBeacon;
	bool arbiterTribunal;
	bool citadelOfAdun;
	bool templarArchives;
	bool haveStorm;
	bool ta;
	bool expandGate;
	bool expandStar;

	bool forceAttack;

	int gwlvl;
	int galvl;

	int noCannons;

	void buildforge(int value = 0);
	void buildShieldbattery();
	void buildcyberneticsCore();
	void buildroboticsFacility();
	void buildroboticsSupportBay();
	void buildobservatory();
	void buildstargate();
	void buildfleetBeacon();
	void buildarbiterTribunal();
	void buildcitadelOfAdun();
	void buildtemplarArchives();


	void PhotonCannons( void );

	Squad* counterSquad;
	
	virtual void counter( std::vector<Squad*>& p_Squad ) = 0;
	int getNoUnit( vector<UnitSetup>& a, UnitType unit );
	vector<BuildplanEntry>* m_CalcBuildPlan;
	int squadID;
	int* m_NoOfWorkers;
	Squad* m_Squad;
};

#endif