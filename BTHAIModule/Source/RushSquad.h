#ifndef __RUSHSQUAD_H__
#define __RUSHSQUAD_H__

#include "Squad.h"
#include "Commander.h"

using namespace BWAPI;
using namespace std;

class RushSquad : public Squad {

public:
	/** Constructor. See Squad.h for more details. */
	RushSquad(int mId, int mType, string mName, int mPriority);
	/** Called each update to issue orders. */
	void computeActions();
private:
	int mMaxDistanceToCenter;
	bool mRushing;
};

#endif
