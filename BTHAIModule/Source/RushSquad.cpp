#include "RushSquad.h"
#include "ExplorationManager.h"

RushSquad::RushSquad(int mId, int mType, string mName, int mPriority)
	: Squad ( mId, mType, mName, mPriority )
{
	buildup = true;
	mMaxDistanceToCenter = 200;
	mMemberGoalsSet = true;
	mRushing = false;
}

void RushSquad::computeActions()
{
	static int lastUnitIndex = getTotalUnits() - 1;

	if (mRushing)
	{
		checkAttack();
		return;
	}

	// Force a rush if the squad is full.
	if (mMemberGoalsSet && getSize() == getTotalUnits())
		mMemberGoalsSet = false;

	// Not attacking, pathfinding is not done and the squad is complete.
	if (!isAttacking() && !mMemberGoalsSet && getSize() == getTotalUnits())
	{
		// Last unit in squad is too far away to start the rush.
		if (agents.at(lastUnitIndex)->getUnit()->getDistance(Position(getCenter())) > mMaxDistanceToCenter)
		{
			//Broodwar->printf("[%d] Don't rush yet! (%d,%d)", Broodwar->getFrameCount(), goal.x(), goal.y());
			agents.at(lastUnitIndex)->setGoal(goal);
		}
		// If the last unit is close enough, move the whole squad.
		else
		{
			TilePosition cGoal = ExplorationManager::getInstance()->getEnemyBaseLocation();
			if(cGoal.x() != -1 && cGoal.y() != -1)
			{
				priority = 1000;
				activePriority = priority;
				//TilePosition cGoal = Commander::getInstance()->getClosestEnemyBuilding(TilePosition(agents.at(0)->getUnit()->getPosition()));
				forceActive();
				attack(cGoal);
				setMemberGoals(cGoal);
				mMemberGoalsSet = true;
				mRushing = true;

			}
		}
	}
	Squad::computeActions();
}