﻿#include "ProtossStrategyPlanner.h"

ProtossStrategyPlanner::ProtossStrategyPlanner( vector<BuildplanEntry>* p_Calcbuilder, int* p_noOfWorkers  )
	: StrategyPlanner( p_Calcbuilder, p_noOfWorkers )

{
	m_CalcBuildPlan->push_back( BuildplanEntry(UnitTypes::Protoss_Pylon,7 ) );
	m_CalcBuildPlan->push_back( BuildplanEntry(UnitTypes::Protoss_Gateway,8 ) );
	m_CalcBuildPlan->push_back( BuildplanEntry(UnitTypes::Protoss_Gateway,8 ) );
	m_CalcBuildPlan->push_back( BuildplanEntry(UnitTypes::Protoss_Pylon,12 ) );
	m_CalcBuildPlan->push_back( BuildplanEntry(UnitTypes::Protoss_Assimilator, 19) );
	buildforge( 20 );	canbuildObs = false;	noCannons = 0;
	*p_noOfWorkers = 10;

};

ProtossStrategyPlanner::~ProtossStrategyPlanner( void )
{
};
void ProtossStrategyPlanner::counter( std::vector<Squad*>& p_Squad )
{
	ExplorationManager *temp = temp->getInstance(  );
	vector<UnitSetup> a = counterSquad->getSetup();

	int units = temp->getNoUnitsByType( UserType::Enemy, UnitTypes::Protoss_Dragoon ) * 2;
	if ( units > 0 ) // counter Dragoons
	{
		int noZ = getNoUnit ( a, UnitTypes::Protoss_Zealot );

		if ( noZ < units*2 )
		{
			counterSquad->addSetup( UnitTypes::Protoss_Zealot, units*2 - noZ);
		}
	}

	units = temp->getNoUnitsByType( UserType::Enemy, UnitTypes::Protoss_High_Templar ) * 2;
	if ( units > 0 ) // counter HTs
	{
		buildtemplarArchives( );

		if ( !haveStorm )
		{
			m_CalcBuildPlan->push_back(BuildplanEntry( TechTypes::Psionic_Storm, 0 ) );
			haveStorm = true;
		}

		int noZ = getNoUnit ( a, UnitTypes::Protoss_Zealot );
		int noDT = getNoUnit ( a, UnitTypes::Protoss_Dark_Templar );;

		if ( noZ < units*2 )
		{
			counterSquad->addSetup( UnitTypes::Protoss_Zealot, units*2 - noZ);
		}
		if ( noDT < units/3 )
		{
			counterSquad->addSetup( UnitTypes::Protoss_Dark_Templar, units/3 - noDT);
		}

	}

	units = temp->getNoUnitsByType( UserType::Enemy, UnitTypes::Protoss_Dark_Templar ) * 2;

	if ( units > 0 || (Broodwar->self()->minerals( ) > 500 && Broodwar->self()->gas( ) > 200 ) ) // counter DTs
	{
		if ( units != 0 )
		{
			int noZ = getNoUnit ( a, UnitTypes::Protoss_Zealot );
			int noO = getNoUnit ( a, UnitTypes::Protoss_Observer );

			if ( noO == 0 )
			{
				counterSquad->addSetup( UnitTypes::Protoss_Observer, 1);
			}
			if ( noZ < units*2 )
			{
				counterSquad->addSetup( UnitTypes::Protoss_Zealot, units*2 - noZ);
			}
		}
	}

	units = temp->getNoUnitsByType( UserType::Enemy, UnitTypes::Protoss_Shuttle ) * 2;
	if ( units > 0 ) // counter shuttles
	{
		buildstargate( );

		int noS= getNoUnit ( a, UnitTypes::Protoss_Scout );

		if ( noS < units )
		{
			counterSquad->addSetup( UnitTypes::Protoss_Scout, units - noS);
		}
	}

	units = temp->getNoUnitsByType( UserType::Enemy, UnitTypes::Protoss_Observer ) * 2;
	if ( units > 0 ) // counter Observers
	{
		buildstargate( );
		buildobservatory( );

		int noS = getNoUnit ( a, UnitTypes::Protoss_Scout );
		int noO = getNoUnit ( a, UnitTypes::Protoss_Observer );

		if ( noS == 0 )
		{
			counterSquad->addSetup( UnitTypes::Protoss_Scout, 1);
		}
		if ( noO == 0 )
		{
			counterSquad->addSetup( UnitTypes::Protoss_Observer, 1);
		}
	}

	units = temp->getNoUnitsByType( UserType::Enemy, UnitTypes::Protoss_Scout ) * 2;
	if ( units > 0 ) // counter scouts
	{
		buildcyberneticsCore( );

		int noD = getNoUnit ( a, UnitTypes::Protoss_Dragoon );

		if ( noD < units )
		{
			counterSquad->addSetup( UnitTypes::Protoss_Dragoon, units - noD);
		}
	}

	units = temp->getNoUnitsByType( UserType::Enemy, UnitTypes::Protoss_Corsair ) * 2;
	if ( units > 0 ) // counter Corsairs
	{
		buildcyberneticsCore( );

		int noD = getNoUnit ( a, UnitTypes::Protoss_Dragoon );

		if ( noD < units*2 )
		{
			counterSquad->addSetup( UnitTypes::Protoss_Dragoon, units*2 - noD);
		}
	}

	units = temp->getNoUnitsByType( UserType::Enemy, UnitTypes::Protoss_Carrier ) * 2;
	if ( units > 0 ) // counter Carriers
	{
		buildstargate( );

		int noS = getNoUnit ( a, UnitTypes::Protoss_Scout );

		if ( noS < units*3 )
		{
			counterSquad->addSetup( UnitTypes::Protoss_Scout, units*3 - noS);
		}
	}

	units = temp->getNoUnitsByType( UserType::Enemy, UnitTypes::Protoss_Arbiter ) * 2;
	if ( units > 0 ) // counter Arbiters
	{
		buildstargate( );
		buildobservatory( );
		int noC = getNoUnit ( a, UnitTypes::Protoss_Corsair );
		int noO = getNoUnit ( a, UnitTypes::Protoss_Observer );

		if ( noC < units )
		{
			counterSquad->addSetup( UnitTypes::Protoss_Scout, units - noC);
		}
		if ( noO == 0 )
		{
			counterSquad->addSetup( UnitTypes::Protoss_Observer, 1);
		}
	}

	units = temp->getNoUnitsByType( UserType::Enemy, UnitTypes::Protoss_Archon ) * 2;
	if ( units > 0 ) // counter Archons
	{

		int noZ = getNoUnit ( a, UnitTypes::Protoss_Zealot );

		if ( noZ < units*2 )
		{
			counterSquad->addSetup( UnitTypes::Protoss_Zealot, units*2 - noZ);
		}
	}
	units = temp->getNoUnitsByType( UserType::Enemy, UnitTypes::Protoss_Dark_Archon ) * 2;
	if ( units > 0 ) // counter Dark Archons
	{
		int noZ = getNoUnit ( a, UnitTypes::Protoss_Zealot );

		if ( noZ < units*2 )
		{
			counterSquad->addSetup( UnitTypes::Protoss_Zealot, units*2 - noZ);
		}
	}
	units = temp->getNoUnitsByType( UserType::Enemy, UnitTypes::Protoss_Reaver ) * 2;
	if ( units > 0 ) // counter Reavers
	{
		int noD = getNoUnit ( a, UnitTypes::Protoss_Dragoon );

		if ( noD < units*2 )
		{
			counterSquad->addSetup( UnitTypes::Protoss_Dragoon, units*2 - noD);
		}
	}
	units = temp->getNoUnitsByType( UserType::Enemy, UnitTypes::Protoss_Zealot ) * 2;
	if ( units > 0 ) // counter Zealots
	{
		int noZ = getNoUnit ( a, UnitTypes::Protoss_Zealot );

		if ( noZ < units )
		{
			counterSquad->addSetup( UnitTypes::Protoss_Zealot, units - noZ);
		}
	}

	if ( Broodwar->self()->minerals( ) > 1000 ) // extra minerals? gate or star expanding
	{
		if ( !expandStar ) 
		{
			m_CalcBuildPlan->push_back(BuildplanEntry(UnitTypes::Protoss_Stargate, 0));
			m_CalcBuildPlan->push_back(BuildplanEntry(UnitTypes::Protoss_Stargate, 0));
			expandStar = true;
		}
		else if ( !expandGate ) 
		{
			m_CalcBuildPlan->push_back(BuildplanEntry(UnitTypes::Protoss_Gateway, 0));
			m_CalcBuildPlan->push_back(BuildplanEntry(UnitTypes::Protoss_Gateway, 0));
			expandGate = true;
		}

	}

	if ( Broodwar->self()->minerals() > 200 ) // extra minerals?
	{
		buildobservatory();

		if ( !canbuildObs ) // create a scout squad see what our enemis are up to
		{
			m_Squad = new ScoutSquad( 29, "obsscout", 1, Squad::AIR );
			m_Squad->addSetup( UnitTypes::Protoss_Observer, 1 );
			m_Squad->setBuildup( false );
			m_Squad->setRequired( true );
			p_Squad.push_back( m_Squad );
			squadID++;
			canbuildObs = true;
		}
		if ( counterSquad->size( ) > 20 ) // big squad? then do some uppgrades!
		{
			if(!forge)
				buildforge( );
			if ( gwlvl < 3 || galvl < 3 )
			{
				m_CalcBuildPlan->push_back(BuildplanEntry(UpgradeTypes::Protoss_Ground_Weapons, 0));
				m_CalcBuildPlan->push_back(BuildplanEntry(UpgradeTypes::Protoss_Ground_Armor, 0));
				gwlvl++;
				galvl++;
			}
		}

		if ( counterSquad->isFull( ) && counterSquad->size( ) > 10 ) // squad is full and we dont know what do do. Arbiter <3 boost
		{
			buildtemplarArchives( );
			if ( !haveStorm )
			{
				m_CalcBuildPlan->push_back(BuildplanEntry(TechTypes::Psionic_Storm, 0));
				haveStorm = true;
			}
			buildarbiterTribunal( );

			int noA = getNoUnit( a, UnitTypes::Protoss_Arbiter );

			if ( noA == 0 )
			{
				counterSquad->addSetup( UnitTypes::Protoss_Arbiter, 1);
			}
		}
	}
}
bool ProtossStrategyPlanner::OpeningStrategy( int p_Supply, std::vector<Squad*>& p_Squad )
{
	if(squadID == 0)
	{

		counterSquad = new Squad(squadID,Squad::OFFENSIVE,"counter",4);
		counterSquad->addSetup(UnitTypes::Protoss_Zealot, 1);
		counterSquad->setBuildup(true);
		counterSquad->setRequired(true);

		p_Squad.push_back(counterSquad);
		squadID++;

	}

	if(p_Supply >= 10 && squadID == 1)
	{
		WorkerAgent* ba;
		ba = (WorkerAgent*)AgentManager::getInstance()->findClosestFreeWorker(Broodwar->self()->getStartLocation());
		ba->clearGoal();
		ba->setState(7);
		m_Squad = new ScoutSquad(squadID,"First Scout",1,Squad::GROUND);
		m_Squad->addSetup(UnitTypes::Protoss_Probe,1);
		m_Squad->addMember(ba);
		m_Squad->setBuildup(true);
		m_Squad->setRequired(false);
		m_Squad->setPriority(1001);

		p_Squad.push_back(m_Squad);

		squadID++;
	}


	else if(p_Supply >= 10 && *m_NoOfWorkers != 27)
	{
		if(squadID == 2)
		{
			m_Squad = new Squad(squadID,Squad::OFFENSIVE,"1th rush",1);
			m_Squad->addSetup( UnitTypes::Protoss_Zealot, 3 );
			m_Squad->setBuildup( true );
			m_Squad->setRequired( true );
			p_Squad.push_back(m_Squad);

			squadID++;

		}
		else
		{
			if(m_Squad->isFull())
				*m_NoOfWorkers = 27;
		}		
	}
	return false;
}
bool ProtossStrategyPlanner::EarlyStrategy( int p_Supply, std::vector<Squad*>& p_Squad )
{
	counter( p_Squad );

	PhotonCannons( );

	if ( (counterSquad->size( ) > 50 && !ta) || (Broodwar->elapsedTime() > 900 && !ta) ) // later game we will have to put some extra pressure on our enemies
	{
		ta = true;
		if(!forge)
			buildforge();
		for ( ; gwlvl < 3; gwlvl++ )
		{
			m_CalcBuildPlan->push_back(BuildplanEntry(UpgradeTypes::Protoss_Ground_Weapons, 0));
		}
		for ( ; galvl < 3; galvl++ )
		{
			m_CalcBuildPlan->push_back(BuildplanEntry(UpgradeTypes::Protoss_Ground_Armor, 0));
		}
		m_Squad = new RushSquad(squadID,Squad::RUSH,"end",1);
		m_Squad->addSetup( UnitTypes::Protoss_Zealot, 10 );
		m_Squad->addSetup( UnitTypes::Protoss_Dragoon, 5 );
		m_Squad->setBuildup( true );
		m_Squad->setRequired( true );
		p_Squad.push_back(m_Squad);

		squadID++;
		m_Squad = new RushSquad(squadID,Squad::RUSH,"end it",2);
		m_Squad->addSetup( UnitTypes::Protoss_Zealot, 15 );
		m_Squad->addSetup( UnitTypes::Protoss_Dragoon, 10 );
		m_Squad->setBuildup( true );
		m_Squad->setRequired( true );
		p_Squad.push_back(m_Squad);

		squadID++;
		m_Squad = new RushSquad(squadID, Squad::RUSH,"end it now!",3);
		m_Squad->addSetup( UnitTypes::Protoss_Zealot, 15 );
		m_Squad->addSetup( UnitTypes::Protoss_Dragoon, 15 );
		m_Squad->setBuildup( true );
		m_Squad->setRequired( true );
		p_Squad.push_back(m_Squad);
		squadID++;

	}

	if( Broodwar->elapsedTime() >= 1500 && !forceAttack) // attack with all if its a long game!
	{
		forceAttack = true;
		Commander::getInstance()->forceAttack();
	}

	return false;
}

bool ProtossStrategyPlanner::MiddleStrategy( int p_Supply, std::vector<Squad*>& p_Squad )
{
	return false;
}

bool ProtossStrategyPlanner::LateStrategy( int p_Supply, std::vector<Squad*>& p_Squad )
{
	return false;
}

void ProtossStrategyPlanner::AllDoneStrategy( int p_Supply, std::vector<Squad*>& p_Squad )
{
}