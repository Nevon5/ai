#include "ArbiterAgent.h"
#include "NavigationAgent.h"

ArbiterAgent::ArbiterAgent(Unit* mUnit)
{
	unit = mUnit;
	type = unit->getType();
	unitID = unit->getID();
	agentType = "ArbiterAgent";
	TeleportGoal = TilePosition(-1,-1 );
	GroupPosition = TeleportGoal;
	goal = TilePosition(-1, -1);
}

void ArbiterAgent::computeActions()
{
	NavigationAgent::getInstance()->computeMove(this, goal, true);
	if ( TeleportGoal != GroupPosition )
	{
		double leng = TeleportGoal.getDistance( this->unit->getTilePosition( ) );
		if ( leng < 100 )
		{
			Position a(GroupPosition);
			this->unit->useTech( TechTypes::Recall, a );
		}
	}
	else if ( this->unit->getEnergy( ) >= TechTypes::Stasis_Field.energyUsed( ) )
	{
		int range = unit->getType().seekRange();
		int enemyInRange = 0;
		vector<Unit*> evilppl;

		if (unit->getType().sightRange() > range)
		{
			range = unit->getType().sightRange();
		}
		
		range += 60;

		for(set<Unit*>::const_iterator i = Broodwar->enemy()->getUnits().begin();
			i!=Broodwar->enemy()->getUnits().end(); i++ ) // TODO: CHECK for organic
		{
			double dist = unit->getPosition().getDistance((*i)->getPosition());
			if (dist <= range)
			{
				evilppl.push_back( (*i) );
				enemyInRange++;
			}
		}


		if ( enemyInRange > 2 )
		{
			int staticstuff = 0;
			int cX = 0;
			int cY = 0;

			for ( int i = 0; i < evilppl.size( ); i++ )
			{
				if ( evilppl[i]->isUnderStorm( ) || evilppl[i]->getStasisTimer() != 0)
				{
					staticstuff++;
				}
				else
				{
					cX += evilppl[i]->getPosition( ).x( );
					cY += evilppl[i]->getPosition( ).y( );
				}
			}

			if ( enemyInRange - staticstuff > 2 )
			{
				cX = cX/(enemyInRange - staticstuff);
				cY = cY/(enemyInRange - staticstuff);

				this->unit->move( Position( cX, cY ) );
				this->unit->useTech( TechTypes::Stasis_Field, Position( cX, cY ) );
			}
		}
	}
}


void ArbiterAgent::teleport( TilePosition from, TilePosition to )
{
	TeleportGoal = to;
	GroupPosition = from;
}