#ifndef __SCOUTSQUAD_H__
#define __SCOUTSQUAD_H__

#include "Squad.h"
#include "Commander.h"
#include "Constructor.h"
#include "Pathfinder.h"
using namespace BWAPI;
using namespace std;

/** This class handle squads used to explore the game world. Any unit type
 * can be used as explorer.
 *
 * Author: Johan Hagelback (johan.hagelback@gmail.com)
 */
class ScoutSquad : public Squad {




private:
	enum ScoutState
	{
		Idle = 0,
		Moving,
		Scouting,
		Escaping,

	};

	ScoutState m_ScoutState;
	

	void checkAlive();
	bool enemyDefenseInRange();
	bool setGoal(TilePosition p_goal);


public:
	/** Constructor. See Squad.h for more details. */
	ScoutSquad(int mId, string mName, int mPriority, int p_Type);

	/** Returns true if this Squad is active, or false if not.
	 * A Squad is active when it first has been filled with agents.
	 * A Squad with destroyed units are still considered Active. */
	bool isActive();

	/** Called each update to issue orders. */
	void computeActions();

	/** Orders this squad to defend a position. */
	void defend(TilePosition mGoal);

	/** Orders this squad to launch an attack at a position. */
	void attack(TilePosition mGoal);

	/** Orders this squad to assist units at a position. */
	void assist(TilePosition mGoal);

	/** Clears the goal for this Squad, i.e. sets the goal
	 * to TilePosition(-1,-1). */
	void clearGoal();

	/** Returns the current goal of this Squad. */
	TilePosition getGoal();

	/** Returns true if this squad has an assigned goal. */
	bool hasGoal();
};

#endif
