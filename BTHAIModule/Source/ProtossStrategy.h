#ifndef __PROTOSSSTRATEGY_H__
#define __PROTOSSSTRATEGY_H__

#include "Commander.h"
#include "Squad.h"
#include "BaseAgent.h"
#include "ScoutSquad.h"
#include "RushSquad.h"
#include "WorkerAgent.h"

#include "ZergStrategyPlanner.h"
#include "ProtossStrategyPlanner.h"
#include "TerranStrategyPlanner.h"

using namespace BWAPI;
using namespace BWTA;
using namespace std;

enum gameTimeStage
{
	opening = 0,
	early,
	middle,
	late,
	allDone
};

/**  
 * Implement your Protoss strategy here.
 */
class ProtossStrategy : public Commander {

private:
	
	Squad* mainSquad;
	ScoutSquad* scoutSquad;
	RushSquad* rushSquad;
	int cSupply;
	int tSupply;
	int min;
	int gas;
	int lastbuildpylon;
	int squadID;

	bool zergRush;
	bool underAttack;
	bool inBattle;
	bool winningBattle;
	bool BuildBuildings;
	bool haveGasPro;
	bool haveSingularityCharge;
	gameTimeStage m_GameTimeStage;
	StrategyPlanner* m_StratPlanner;

	
public:
	ProtossStrategy();

	/** Destructor. */
	~ProtossStrategy();

	/** Called each update to issue orders. */
	virtual void computeActions();

	/** Returns the unique id for this strategy. */
	static string getStrategyId()
	{
		return "ProtossStrategy";
	}

	void addUnitToBuildPlan( BuildplanEntry p_BuildPlanEntry );

	void AICommander( );

	void CalcWhatToBuild( );
};

#endif
