#include "HighTemplarAgent.h"
#include "NavigationAgent.h"
#include "AgentManager.h"
#include "Commander.h"

HighTemplarAgent::HighTemplarAgent(Unit* mUnit)
{
	unit = mUnit;
	type = unit->getType();
	unitID = unit->getID();
	agentType = "HighTemplarAgent";

	goal = TilePosition(-1, -1);
}

void HighTemplarAgent::computeActions()
{
	NavigationAgent::getInstance()->computeMove(this, goal, true);
	if ( this->unit->getEnergy( ) >= TechTypes::Psionic_Storm.energyUsed( ) )
	{
		int range = unit->getType().seekRange();
		int enemyInRange = 0;
		vector<Unit*> evilppl;
		if (unit->getType().sightRange() > range)
		{
			range = unit->getType().sightRange();
		}

		range += 60;

		for(set<Unit*>::const_iterator i = Broodwar->enemy()->getUnits().begin();
			i!=Broodwar->enemy()->getUnits().end(); i++ ) // TODO: CHECK for organic
		{
			double dist = unit->getPosition().getDistance((*i)->getPosition());
			if (dist <= range)
			{
				evilppl.push_back( (*i) );
				enemyInRange++;
			}
		}

		if ( enemyInRange > 3 )
		{
			int stormstuff = 0;
			int cX = 0;
			int cY = 0;

			for ( int i = 0; i < evilppl.size( ); i++ )
			{
				if ( evilppl[i]->getType( ) == UnitTypes::Zerg_Ultralisk )
				{
					this->unit->useTech( TechTypes::Psionic_Storm,  evilppl[i]->getPosition( ) );
					return;
				}	
				if ( evilppl[i]->isUnderStorm( ) || evilppl[i]->getStasisTimer() != 0)
				{
					stormstuff++;
				}
				else
				{
					cX += evilppl[i]->getPosition( ).x( );
					cY += evilppl[i]->getPosition( ).y( );
				}
			}

			if ( enemyInRange - stormstuff > 3 )
			{
				cX = cX/(enemyInRange - stormstuff);
				cY = cY/(enemyInRange - stormstuff);
				this->unit->move( Position( cX, cY ) );
				this->unit->useTech( TechTypes::Psionic_Storm, Position( cX, cY ) );
			}
		}
	}
	else
	{
		int range = unit->getType().seekRange();
		int enemyInRange = 0;
		vector<Unit*> godppl;

		if (unit->getType().sightRange() > range)
		{
			range = unit->getType().sightRange();
		}


		for(set<Unit*>::const_iterator i = Broodwar->self()->getUnits().begin();
			i!=Broodwar->self()->getUnits().end(); i++ ) // TODO: CHECK for organic
		{
			double dist = unit->getPosition().getDistance((*i)->getPosition());
			if (dist <= range)
			{
				godppl.push_back( (*i) );
				enemyInRange++;
			}
		}
		Unit* target = NULL;
		for ( int i = 0; i < godppl.size( ); i++ )
		{
			if ( godppl[i]->getType().getName( ) == this->unit->getType( ).getName( ) )
			{
				target = godppl[i];
				break;
			}
		}
		if ( target != NULL )
		{
			if ( this->unit->getHitPoints( ) < 5 ) 
			{
				this->unit->useTech( TechTypes::Archon_Warp,  target);
			}
		}
	}

}
