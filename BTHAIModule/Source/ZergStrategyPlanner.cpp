#include "ZergStrategyPlanner.h"


ZergStrategyPlanner::ZergStrategyPlanner( vector<BuildplanEntry>* p_Calcbuilder, int* p_noOfWorkers )
	: StrategyPlanner( p_Calcbuilder, p_noOfWorkers )
{

	m_CalcBuildPlan->push_back(BuildplanEntry(UnitTypes::Protoss_Pylon, 0));
	m_CalcBuildPlan->push_back(BuildplanEntry(UnitTypes::Protoss_Gateway, 0)); 
	m_CalcBuildPlan->push_back(BuildplanEntry(UnitTypes::Protoss_Pylon, 0));
	m_CalcBuildPlan->push_back(BuildplanEntry(UnitTypes::Protoss_Gateway, 12)); 
	m_CalcBuildPlan->push_back(BuildplanEntry(UnitTypes::Protoss_Gateway, 12)); 
	m_CalcBuildPlan->push_back(BuildplanEntry(UnitTypes::Protoss_Assimilator, 0));
	buildforge( 20 );

	haveStorm = false;
	expandGate = false;
	expandStar = false;
	canbuildObs = false;

	gwlvl = 0;
	galvl = 0;
};

ZergStrategyPlanner::~ZergStrategyPlanner( void )
{
};

void ZergStrategyPlanner::counter( std::vector<Squad*>& p_Squad )
{
	ExplorationManager *temp = temp->getInstance(  );
	vector<UnitSetup> a = counterSquad->getSetup( );
	int units = temp->getNoUnitsByType( UserType::Enemy, UnitTypes::Zerg_Zergling )*2;

	if ( units > 0 )
	{
		// counter Zerg_Zergling
		int noZ = getNoUnit( a, UnitTypes::Protoss_Zealot );
		int noHT = getNoUnit( a, UnitTypes::Protoss_High_Templar );

		if ( noZ < units/2 )
		{
			counterSquad->addSetup( UnitTypes::Protoss_Zealot, units/2 - noZ);
		}

		if ( noHT < units/10 )
		{
			buildtemplarArchives( );

			if ( !haveStorm )
			{
				m_CalcBuildPlan->push_back(BuildplanEntry(TechTypes::Psionic_Storm, 0));
				haveStorm = true;
			}
			counterSquad->addSetup( UnitTypes::Protoss_High_Templar, units - noHT);
		}
		// info http://classic.battle.net/scc/zerg/units/zergling.shtml
	}

	units = temp->getNoUnitsByType( UserType::Enemy, UnitTypes::Zerg_Hydralisk )*2;
	if ( units > 0 )
	{
		buildcyberneticsCore( );

		// counter Zerg_Hydralisk
		int noD = getNoUnit( a, UnitTypes::Protoss_Dragoon );
		int noZ = getNoUnit( a, UnitTypes::Protoss_Zealot ); 

		if ( noZ < units/2 )
		{
			counterSquad->addSetup( UnitTypes::Protoss_Zealot, units/2 - noZ);
		}
		if ( noD < units/2 )
		{
			counterSquad->addSetup( UnitTypes::Protoss_Dragoon, units/2 - noD);
		}
		// info http://classic.battle.net/scc/zerg/units/hydralisk.shtml
	}

	units = temp->getNoUnitsByType( UserType::Enemy, UnitTypes::Zerg_Lurker )*2;

	if ( units > 0 || !canbuildObs && (Broodwar->self()->minerals( ) > 100 && Broodwar->self()->gas( ) > 100 ) )
	{
		buildobservatory();

		if ( !canbuildObs )
		{
			m_Squad = new ScoutSquad( squadID, "obsscout", 1, Squad::AIR );
			m_Squad->addSetup( UnitTypes::Protoss_Observer, 1 );
			m_Squad->setBuildup( false );
			m_Squad->setRequired( true );
			p_Squad.push_back( m_Squad );
			squadID++;
			canbuildObs = true;
		}
		if ( units != 0 )
		{
			int noO = getNoUnit( a, UnitTypes::Protoss_Observer );

			if ( noO == 0 )
			{
				counterSquad->addSetup( UnitTypes::Protoss_Observer, 1);
			}
		}
		// counter Zerg_Lurker
		// info http://classic.battle.net/scc/zerg/units/lurker.shtml
	}

	units = temp->getNoUnitsByType( UserType::Enemy, UnitTypes::Zerg_Queen )*2;
	if ( units > 0 )
	{
		// counter Zerg_Queen
		buildstargate();

		int noS = getNoUnit( a, UnitTypes::Protoss_Scout );

		if ( noS < units/2 )
		{
			counterSquad->addSetup( UnitTypes::Protoss_Scout, units/2 - noS);
		}
		// info http://classic.battle.net/scc/zerg/units/queen.shtml
	}

	units = temp->getNoUnitsByType( UserType::Enemy, UnitTypes::Zerg_Defiler )*2;
	if ( units > 0 )
	{
		// counter Zerg_Defiler
		// who would ever use them?
		// info http://classic.battle.net/scc/zerg/units/defiler.shtml
	}

	units += temp->getNoUnitsByType( UserType::Enemy, UnitTypes::Zerg_Ultralisk )*2 ;
	if ( units > 0 )
	{
		// counter Zerg_Ultralisk
		buildtemplarArchives();
		if ( !haveStorm )
		{
			m_CalcBuildPlan->push_back(BuildplanEntry(TechTypes::Psionic_Storm, 0));
			haveStorm = true;
		}

		int noHT = getNoUnit( a, UnitTypes::Protoss_High_Templar );
		int noZ = getNoUnit( a, UnitTypes::Protoss_Zealot );

		if ( noHT < units/2 )
		{
			counterSquad->addSetup( UnitTypes::Protoss_High_Templar, units/2 - noHT);
		}
		if ( noZ < units*3 )
		{
			counterSquad->addSetup( UnitTypes::Protoss_Zealot, units*3 - noZ);
		}
		// info http://classic.battle.net/scc/zerg/units/ultralisk.shtml
	}

	units = temp->getNoUnitsByType( UserType::Enemy, UnitTypes::Zerg_Mutalisk )*2;
	if ( units > 0 )
	{
		buildstargate( );
		buildtemplarArchives( );

		if ( !haveStorm )
		{
			m_CalcBuildPlan->push_back(BuildplanEntry(TechTypes::Psionic_Storm, 0));
			haveStorm = true;
		}

		int noC = getNoUnit( a, UnitTypes::Protoss_Corsair );
		int noHT = getNoUnit( a, UnitTypes::Protoss_High_Templar );

		if ( noC < units )
		{
			counterSquad->addSetup( UnitTypes::Protoss_Corsair, units - noC);
		}
		if ( noHT < units )
		{
			counterSquad->addSetup( UnitTypes::Protoss_High_Templar, units - noHT);
		}
		// info http://classic.battle.net/scc/zerg/units/mutalisk.shtml
	}

	units = temp->getNoUnitsByType( UserType::Enemy, UnitTypes::Zerg_Scourge )*2;
	if ( units > 0 )
	{
		// counter  Zerg_Scourge
		int noD = getNoUnit( a, UnitTypes::Protoss_Dragoon );

		if ( noD < units )
		{
			counterSquad->addSetup( UnitTypes::Protoss_Dragoon, units - noD);
		}
		// info http://classic.battle.net/scc/zerg/units/scourge.shtml
	}

	units = temp->getNoUnitsByType( UserType::Enemy, UnitTypes::Zerg_Guardian )*2;
	if ( units > 0 )
	{
		// counter Zerg_Guardian
		buildstargate( );

		int noS = getNoUnit( a, UnitTypes::Protoss_Scout );

		if ( noS < units/2 )
		{
			counterSquad->addSetup( UnitTypes::Protoss_Scout, units/2 - noS);
		}
		// info http://classic.battle.net/scc/zerg/units/guardian.shtml
	}

	units = temp->getNoUnitsByType( UserType::Enemy, UnitTypes::Zerg_Devourer )*2;
	if ( units > 0 )
	{
		// counter Zerg_Devourer
		buildstargate( );

		int noS = getNoUnit( a, UnitTypes::Protoss_Scout );

		if ( noS < units/2 )
		{
			counterSquad->addSetup( UnitTypes::Protoss_Scout, units/2 - noS);
		}
		// info http://classic.battle.net/scc/zerg/units/devourer.shtml
	}

	if ( Broodwar->self()->minerals( ) > 1000 )
	{
		if ( !expandStar ) 
		{
			m_CalcBuildPlan->push_back(BuildplanEntry(UnitTypes::Protoss_Stargate, 0));
			m_CalcBuildPlan->push_back(BuildplanEntry(UnitTypes::Protoss_Stargate, 0));
			expandStar = true;
		}
		else if ( !expandGate ) 
		{
			m_CalcBuildPlan->push_back(BuildplanEntry(UnitTypes::Protoss_Gateway, 0));
			m_CalcBuildPlan->push_back(BuildplanEntry(UnitTypes::Protoss_Gateway, 0));
			expandGate = true;
		}

	}

	if ( gwlvl == 0 )
	{
		if ( forge && Broodwar->self()->minerals() > 200 && Broodwar->self()->gas() > 200 && gwlvl )
		{
			m_CalcBuildPlan->push_back(BuildplanEntry(UpgradeTypes::Protoss_Ground_Weapons, 0));
			gwlvl++;
		}
	}
	else if ( galvl == 0 )
	{
		if ( forge && Broodwar->self()->minerals( ) > 200 && Broodwar->self()->gas( ) > 200 )
		{
			m_CalcBuildPlan->push_back(BuildplanEntry(UpgradeTypes::Protoss_Ground_Armor, 0));
			galvl++;
		}
	}

	if ( Broodwar->self()->minerals() > 200 )
	{
		if ( counterSquad->isFull( ) && counterSquad->size( ) > 10 )
		{
			buildstargate( );
			buildtemplarArchives( );

			if ( !haveStorm )
			{
				m_CalcBuildPlan->push_back(BuildplanEntry(TechTypes::Psionic_Storm, 0));
				haveStorm = true;
			}
			if ( !arbiterTribunal )
			{
				m_CalcBuildPlan->push_back(BuildplanEntry(UnitTypes::Protoss_Arbiter_Tribunal, 0));
				m_CalcBuildPlan->push_back(BuildplanEntry(TechTypes::Stasis_Field, 0));
				arbiterTribunal = true;
			}
			int noA = getNoUnit( a, UnitTypes::Protoss_Arbiter );

			if ( noA == 0 )
			{
				counterSquad->addSetup( UnitTypes::Protoss_Arbiter, 1);
			}
		}
	}

}

bool ZergStrategyPlanner::OpeningStrategy( int p_Supply, std::vector<Squad*>& p_Squad )
{
	if(p_Supply >= 10 && squadID == 0)
	{
		WorkerAgent* ba;
		ba = (WorkerAgent*)AgentManager::getInstance()->findClosestFreeWorker(Broodwar->self()->getStartLocation());
		ba->clearGoal();
		ba->setState(7);
		m_Squad = new ScoutSquad(squadID,"First Scout",1,Squad::GROUND);
		m_Squad->addSetup(UnitTypes::Protoss_Probe,1);
		m_Squad->addMember(ba);
		m_Squad->setBuildup(true);
		m_Squad->setRequired(false);
		m_Squad->setPriority(1001);
		squadID++;
		counterSquad = new Squad( squadID, Squad::OFFENSIVE, "Counter1", 4  );
		counterSquad->addSetup( UnitTypes::Protoss_Zealot, 1 );
		counterSquad->setBuildup( false );
		counterSquad->setRequired( true );
		p_Squad.push_back( counterSquad );
		squadID++;


		m_Squad = new RushSquad( squadID, Squad::RUSH, "kill them", 3 );
		m_Squad->addSetup( UnitTypes::Protoss_Zealot, 10 );

		m_Squad->setBuildup( true );
		m_Squad->setRequired( true );
		p_Squad.push_back( m_Squad );
		squadID++;

		return true; //Return true when this stage is done
	}


	return false;
}

bool ZergStrategyPlanner::EarlyStrategy( int p_Supply, std::vector<Squad*>& p_Squad )
{
	counter( p_Squad );

	PhotonCannons();

	if ( (counterSquad->size( ) > 50 && !ta) || (Broodwar->elapsedTime() > 900 && !ta) )

	{
		ta = true;
		if(!forge)
			buildforge();
		for ( ; gwlvl < 3; gwlvl++ )
		{
			m_CalcBuildPlan->push_back(BuildplanEntry(UpgradeTypes::Protoss_Ground_Weapons, 0));
		}
		for ( ; galvl < 3; galvl++ )
		{
			m_CalcBuildPlan->push_back(BuildplanEntry(UpgradeTypes::Protoss_Ground_Armor, 0));
		}
		m_Squad = new RushSquad(squadID,Squad::RUSH,"end",1);
		m_Squad->addSetup( UnitTypes::Protoss_Zealot, 10 );
		m_Squad->addSetup( UnitTypes::Protoss_Dragoon, 5 );
		m_Squad->setBuildup( true );
		m_Squad->setRequired( true );
		p_Squad.push_back(m_Squad);

		squadID++;
		m_Squad = new RushSquad(squadID,Squad::RUSH,"end it",2);
		m_Squad->addSetup( UnitTypes::Protoss_Zealot, 15 );
		m_Squad->addSetup( UnitTypes::Protoss_Dragoon, 10 );
		m_Squad->setBuildup( true );
		m_Squad->setRequired( true );
		p_Squad.push_back(m_Squad);

		squadID++;
		m_Squad = new RushSquad(squadID, Squad::RUSH,"end it now!",3);
		m_Squad->addSetup( UnitTypes::Protoss_Zealot, 15 );
		m_Squad->addSetup( UnitTypes::Protoss_Dragoon, 15 );
		m_Squad->setBuildup( true );
		m_Squad->setRequired( true );
		p_Squad.push_back(m_Squad);
		squadID++;

	}

	if( Broodwar->elapsedTime() >= 1500 && !forceAttack)
	{
		forceAttack = true;
		Commander::getInstance()->forceAttack();
	}

	return false;
}

bool ZergStrategyPlanner::MiddleStrategy( int p_Supply, std::vector<Squad*>& p_Squad )
{
	return false;
}

bool ZergStrategyPlanner::LateStrategy( int p_Supply, std::vector<Squad*>& p_Squad )
{
	return false;
}

void ZergStrategyPlanner::AllDoneStrategy( int p_Supply, std::vector<Squad*>& p_Squad )
{
}