#include "DarkTemplarAgent.h"
#include "NavigationAgent.h"
#include "TargetingAgent.h"

DarkTemplarAgent::DarkTemplarAgent(Unit* mUnit)
{
	unit = mUnit;
	type = unit->getType();
	unitID = unit->getID();
	agentType = "DarkTemplarAgent";
	
	goal = TilePosition(-1, -1);
}

void DarkTemplarAgent::computeActions()
{
	NavigationAgent::getInstance()->computeMove(this, goal, false);

	int range = unit->getType().seekRange();
	int enemyInRange = 0;
	vector<Unit*> godppl;
	if ( this->unit->getHitPoints( ) < 5 )
	{
		for(set<Unit*>::const_iterator i = Broodwar->self()->getUnits().begin();
		i!=Broodwar->self()->getUnits().end(); i++ ) // TODO: CHECK for organic
		{
			double dist = unit->getPosition().getDistance((*i)->getPosition());
			if (dist <= range)
			{
				godppl.push_back( (*i) );
				enemyInRange++;
			}
		}
		Unit* target = NULL;
		for ( int i = 0; i < godppl.size( ); i++ )
		{
			if ( godppl[i]->getType().getName( ) == this->unit->getType( ).getName( ) )
			{
				target = godppl[i];
				break;
			}
		}
		if ( target != NULL )
		{
			this->unit->useTech( TechTypes::Dark_Archon_Meld,  target);
		}
	}
}
