#include "ExplorationManager.h"
#include "AgentManager.h"
#include "BuildingPlacer.h"

ExplorationManager* ExplorationManager::instance = NULL;

ExplorationManager::ExplorationManager()
{
	active = true;

	resetEnemyForce();
	resetPlayerForce();

	//Add the regions for this map
	for(set<BWTA::Region*>::const_iterator i=getRegions().begin();i!=getRegions().end();i++)
	{
		exploreData.push_back(ExploreData((*i)->getCenter()));
	}

	siteSetFrame = 0;

	lastCallFrame = Broodwar->getFrameCount();

	expansionSite = TilePosition(-1, -1);
	m_EnemyBase = TilePosition(-1,-1);
}

ExplorationManager::~ExplorationManager()
{
	instance = NULL;
}

void ExplorationManager::Initialize(void)
{
	playerForces = initP();
	playerForces.totGround = 0;
	playerForces.totAir = 0;
	playerForces.totBuildings = 0;
	playerForces.airAttackStr = 0;
	playerForces.groundAttackStr = 0;
	playerForces.airDefenseStr = 0;
	playerForces.groundDefenseStr = 0;
	playerForces.race = Races::Protoss.getID();
	InitializeEnemy();
}
void ExplorationManager::InitializeEnemy()
{
	if(enemyIsProtoss())
		enemyForces = initP();
	else if(enemyIsTerran())
		enemyForces = initT();
	else if(enemyIsZerg())
		enemyForces = initZ();

	if(enemyForces.initialized)
	{
		enemyForces.totGround = 0;
		enemyForces.totAir = 0;
		enemyForces.totBuildings = 0;
		enemyForces.airAttackStr = 0;
		enemyForces.groundAttackStr = 0;
		enemyForces.airDefenseStr = 0;
		enemyForces.groundDefenseStr = 0;
	}
}
PlayerForce ExplorationManager::initP(void)
{
	PlayerForce pf;

	pf.groundForce.insert ( std::pair<int,int>( UnitTypes::Protoss_Probe.getID(),0));
	pf.groundForce.insert ( std::pair<int,int>( UnitTypes::Protoss_Zealot.getID(),0));
	pf.groundForce.insert ( std::pair<int,int>( UnitTypes::Protoss_Dragoon.getID(),0));
	pf.groundForce.insert ( std::pair<int,int>( UnitTypes::Protoss_High_Templar.getID(),0));
	pf.groundForce.insert ( std::pair<int,int>( UnitTypes::Protoss_Dark_Templar.getID(),0));
	pf.groundForce.insert ( std::pair<int,int>( UnitTypes::Protoss_Reaver.getID(),0));
	pf.groundForce.insert ( std::pair<int,int>( UnitTypes::Protoss_Archon.getID(),0));
	pf.groundForce.insert ( std::pair<int,int>( UnitTypes::Protoss_Dark_Archon.getID(),0));
	pf.groundForce.insert ( std::pair<int,int>( UnitTypes::Protoss_Interceptor.getID(),0));
	pf.groundForce.insert ( std::pair<int,int>( UnitTypes::Protoss_Scarab.getID(),0));

	pf.airForce.insert ( std::pair<int,int>( UnitTypes::Protoss_Shuttle.getID(),0));
	pf.airForce.insert ( std::pair<int,int>( UnitTypes::Protoss_Observer.getID(),0));
	pf.airForce.insert ( std::pair<int,int>( UnitTypes::Protoss_Scout.getID(),0));
	pf.airForce.insert ( std::pair<int,int>( UnitTypes::Protoss_Corsair.getID(),0));
	pf.airForce.insert ( std::pair<int,int>( UnitTypes::Protoss_Carrier.getID(),0));
	pf.airForce.insert ( std::pair<int,int>( UnitTypes::Protoss_Arbiter.getID(),0));

	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Protoss_Assimilator.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Protoss_Nexus.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Protoss_Pylon.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Protoss_Gateway.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Protoss_Forge.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Protoss_Shield_Battery.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Protoss_Cybernetics_Core.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Protoss_Photon_Cannon.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Protoss_Robotics_Facility.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Protoss_Stargate.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Protoss_Citadel_of_Adun.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Protoss_Robotics_Support_Bay.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Protoss_Fleet_Beacon.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Protoss_Templar_Archives.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Protoss_Observatory.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Protoss_Arbiter_Tribunal.getID(),0));

	pf.race = Races::Protoss.getID();
	pf.initialized = true;

	return pf;
}
PlayerForce ExplorationManager::initT(void)
{
	PlayerForce pf;

	pf.groundForce.insert ( std::pair<int,int>( UnitTypes::Terran_SCV.getID(),0));
	pf.groundForce.insert ( std::pair<int,int>( UnitTypes::Terran_Marine.getID(),0));
	pf.groundForce.insert ( std::pair<int,int>( UnitTypes::Terran_Firebat.getID(),0));
	pf.groundForce.insert ( std::pair<int,int>( UnitTypes::Terran_Ghost.getID(),0));
	pf.groundForce.insert ( std::pair<int,int>( UnitTypes::Terran_Vulture.getID(),0));
	pf.groundForce.insert ( std::pair<int,int>( UnitTypes::Terran_Siege_Tank_Siege_Mode.getID(),0));
	pf.groundForce.insert ( std::pair<int,int>( UnitTypes::Terran_Siege_Tank_Tank_Mode.getID(),0));
	pf.groundForce.insert ( std::pair<int,int>( UnitTypes::Terran_Medic.getID(),0));
	pf.groundForce.insert ( std::pair<int,int>( UnitTypes::Terran_Goliath.getID(),0));
	pf.groundForce.insert ( std::pair<int,int>( UnitTypes::Terran_Vulture_Spider_Mine.getID(),0 ));

	pf.airForce.insert ( std::pair<int,int>( UnitTypes::Terran_Wraith.getID(),0));
	pf.airForce.insert ( std::pair<int,int>( UnitTypes::Terran_Dropship.getID(),0));
	pf.airForce.insert ( std::pair<int,int>( UnitTypes::Terran_Battlecruiser.getID(),0));
	pf.airForce.insert ( std::pair<int,int>( UnitTypes::Terran_Science_Vessel.getID(),0));
	pf.airForce.insert ( std::pair<int,int>( UnitTypes::Terran_Valkyrie.getID(),0));
	pf.airForce.insert ( std::pair<int,int>( UnitTypes::Terran_Nuclear_Missile.getID(),0));

	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Terran_Supply_Depot.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Terran_Command_Center.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Terran_Comsat_Station.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Terran_Nuclear_Silo.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Terran_Engineering_Bay.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Terran_Barracks.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Terran_Refinery.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Terran_Missile_Turret.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Terran_Academy.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Terran_Bunker.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Terran_Factory.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Terran_Machine_Shop.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Terran_Starport.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Terran_Control_Tower.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Terran_Armory.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Terran_Science_Facility.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Terran_Physics_Lab.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Terran_Covert_Ops.getID(),0));

	pf.race = Races::Terran.getID();
	pf.initialized = true;

	return pf;
}
PlayerForce ExplorationManager::initZ(void)
{
	PlayerForce pf;
	
	pf.groundForce.insert ( std::pair<int,int>( UnitTypes::Zerg_Larva.getID(),0));
	pf.groundForce.insert ( std::pair<int,int>( UnitTypes::Zerg_Egg.getID(),0));
	pf.groundForce.insert ( std::pair<int,int>( UnitTypes::Zerg_Drone.getID(),0));
	pf.groundForce.insert ( std::pair<int,int>( UnitTypes::Zerg_Zergling.getID(),0));
	pf.groundForce.insert ( std::pair<int,int>( UnitTypes::Zerg_Hydralisk.getID(),0));
	pf.groundForce.insert ( std::pair<int,int>( UnitTypes::Zerg_Lurker.getID(),0));
	pf.groundForce.insert ( std::pair<int,int>( UnitTypes::Zerg_Lurker_Egg.getID(),0));
	pf.groundForce.insert ( std::pair<int,int>( UnitTypes::Zerg_Defiler.getID(),0));
	pf.groundForce.insert ( std::pair<int,int>( UnitTypes::Zerg_Ultralisk.getID(),0));
	pf.groundForce.insert ( std::pair<int,int>( UnitTypes::Zerg_Broodling.getID(),0));
	pf.groundForce.insert ( std::pair<int,int>( UnitTypes::Zerg_Infested_Terran.getID(),0));

	pf.airForce.insert ( std::pair<int,int>( UnitTypes::Zerg_Overlord.getID(),0));
	pf.airForce.insert ( std::pair<int,int>( UnitTypes::Zerg_Mutalisk.getID(),0));
	pf.airForce.insert ( std::pair<int,int>( UnitTypes::Zerg_Scourge.getID(),0));
	pf.airForce.insert ( std::pair<int,int>( UnitTypes::Zerg_Guardian.getID(),0));
	pf.airForce.insert ( std::pair<int,int>( UnitTypes::Zerg_Devourer.getID(),0));
	pf.airForce.insert ( std::pair<int,int>( UnitTypes::Zerg_Cocoon.getID(),0));
	pf.airForce.insert ( std::pair<int,int>( UnitTypes::Zerg_Queen.getID(),0));

	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Zerg_Extractor.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Zerg_Hatchery.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Zerg_Evolution_Chamber.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Zerg_Creep_Colony.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Zerg_Spawning_Pool.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Zerg_Spore_Colony.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Zerg_Sunken_Colony.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Zerg_Hydralisk_Den.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Zerg_Lair.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Zerg_Spire.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Zerg_Queens_Nest.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Zerg_Greater_Spire.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Zerg_Hive.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Zerg_Nydus_Canal.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Zerg_Defiler_Mound.getID(),0));
	pf.buildings.insert ( std::pair<int,int>( UnitTypes::Zerg_Ultralisk_Cavern.getID(),0));


	pf.race = Races::Zerg.getID();
	pf.initialized = true;

	return pf;
}

TilePosition ExplorationManager::getEnemyBaseLocation( void )
{
	return m_EnemyBase;
}
void ExplorationManager::setEnemyBaseLocation( TilePosition pos )
{
	m_EnemyBase = pos;
}

void ExplorationManager::setInactive()
{
	active = false;
}

bool ExplorationManager::isActive()
{
	return active;
}

ExplorationManager* ExplorationManager::getInstance()
{
	if (instance == NULL)
	{
		instance = new ExplorationManager();
	}
	return instance;
}

void ExplorationManager::computeActions()
{
	if (!active)
	{
		return;
	}

	if (Broodwar->getFrameCount() % 50 == 0)
	{
		if(!enemyForces.initialized)
			InitializeEnemy();

		calcEnemyForceData();
		calcOwnForceData();
	}
}

TilePosition ExplorationManager::searchExpansionSite()
{
	getExpansionSite();

	if (expansionSite.x() == -1)
	{
		expansionSite = BuildingPlacer::getInstance()->findExpansionSite();
		siteSetFrame = Broodwar->getFrameCount();
	}

	return expansionSite;
}

TilePosition ExplorationManager::getExpansionSite()
{
	if (expansionSite.x() >= 0)
	{
		if (Broodwar->getFrameCount() - siteSetFrame > 500)
		{
			expansionSite = TilePosition(-1, -1);
		}
	}

	return expansionSite;
}

void ExplorationManager::setExpansionSite(TilePosition pos)
{
	if (pos.x() >= 0)
	{
		siteSetFrame = Broodwar->getFrameCount();
		expansionSite = pos;
	}
}

TilePosition ExplorationManager::getNextToExplore(Squad* squad)
{
	TilePosition curPos = squad->getCenter();
	TilePosition goal = squad->getGoal();

	//Special case: No goal set
	if (goal.x() == -1 || goal.y() == -1)
	{
		BWTA::Region* startRegion = getRegion(curPos); 
		goal = TilePosition(startRegion->getCenter());
		return goal;
	}

	double dist = curPos.getDistance(goal);

	double acceptDist = 4;
	if (squad->isGround())
	{
		acceptDist = 6;
	}

	if (dist <= acceptDist)
	{
		//Squad is close to goal

		//1. Set region to explored
		setExplored(goal);

		//2. Find new region to explore
		BWTA::Region* startRegion = getRegion(goal);
		BWTA::Region* bestRegion = startRegion;

		if (bestRegion != NULL)
		{
			int bestLastVisitFrame = getLastVisitFrame(bestRegion);

			if (!squad->isAir())
			{
				//Ground explorers
				for(set<BWTA::Region*>::const_iterator i=startRegion->getReachableRegions().begin();i!=startRegion->getReachableRegions().end();i++)
				{
					int cLastVisitFrame = getLastVisitFrame((*i));
					TilePosition c = TilePosition((*i)->getCenter());
					if (cLastVisitFrame <= bestLastVisitFrame)
					{
						bestLastVisitFrame = cLastVisitFrame;
						bestRegion = (*i);
					}
				}
			}
			else
			{
				//Air explorers
				double bestDist = 100000;
				for(set<BWTA::Region*>::const_iterator i=getRegions().begin();i!=getRegions().end();i++)
				{
					int cLastVisitFrame = getLastVisitFrame((*i));
					TilePosition c = TilePosition((*i)->getCenter());
					double dist = c.getDistance(curPos);
					if (cLastVisitFrame < bestLastVisitFrame)
					{
						bestLastVisitFrame = cLastVisitFrame;
						bestRegion = (*i);
						bestDist = dist;
					}
					if (cLastVisitFrame == bestLastVisitFrame && dist < bestDist)
					{
						bestLastVisitFrame = cLastVisitFrame;
						bestRegion = (*i);
						bestDist = dist;
					}
				}
			}

			TilePosition newGoal = TilePosition(bestRegion->getCenter());
			return newGoal;
		}
	}

	return TilePosition(-1, -1);
}

void ExplorationManager::setExplored(TilePosition goal)
{
	bool found = false;
	for (int i = 0; i < (int)exploreData.size(); i++)
	{
		if (exploreData.at(i).matches(goal))
		{
			exploreData.at(i).lastVisitFrame = Broodwar->getFrameCount();
			found = true;
		}
	}
}

int ExplorationManager::getLastVisitFrame(BWTA::Region* region)
{
	for (int i = 0; i < (int)exploreData.size(); i++)
	{
		if (exploreData.at(i).matches(region))
		{

			//Check if region is visible. If so, set lastVisitFrame to now
			if (Broodwar->isVisible(exploreData.at(i).center))
			{
				exploreData.at(i).lastVisitFrame = Broodwar->getFrameCount();
			}

			return exploreData.at(i).lastVisitFrame;
		}
	}

	//Error: No region found
	TilePosition goal = TilePosition(region->getCenter());
	return -1;
}

void ExplorationManager::showIntellData()
{
	if(!enemyForces.initialized)
		return;

	int a = getNoUnitsByType(Enemy,UnitTypes::Terran_Marine);
	int b =  UnitTypes::Terran_Marine.destroyScore();
	//Broodwar->drawTextScreen(10,60, "NoOfMarines * destroyScore = totalScore: %i * %i = %i",a,b,a*b);

	a = getNoUnitsByType(Enemy,UnitTypes::Terran_Firebat);
	b = UnitTypes::Terran_Firebat.destroyScore();
	//Broodwar->drawTextScreen(10,75, "NoOfFirebat * destroyScore = totalScore: %i * %i = %i",a,b,a*b);

	a = getNoUnitsByType(Enemy,UnitTypes::Terran_Medic);
	b = UnitTypes::Terran_Medic.destroyScore();
	//Broodwar->drawTextScreen(10,90, "NoOfMedic * destroyScore = totalScore: %i * %i = %i",a,b,a*b);


	a = getNoUnitsByType(Self,UnitTypes::Protoss_Dragoon);
	b = UnitTypes::Protoss_Dragoon.destroyScore();
	//Broodwar->drawTextScreen(10,120, "NoOfDragoon * destroyScore = totalScore: %i * %i = %i",a,b,a*b);

	//Broodwar->drawTextScreen(10,60, "Enemy Marines: %i (%i)", getNoUnitsByType(Enemy,UnitTypes::Terran_Marine), UnitTypes::Terran_Marine.destroyScore() );
	//Broodwar->drawTextScreen(10,75, "Enemy Firebats: %i (%i)", getNoUnitsByType(Enemy,UnitTypes::Terran_Firebat), UnitTypes::Terran_Firebat.destroyScore() );
	//Broodwar->drawTextScreen(10,90, "Enemy Medics: %i (%i)", getNoUnitsByType(Enemy,UnitTypes::Terran_Medic), UnitTypes::Terran_Medic.destroyScore() );

	//Broodwar->drawTextScreen(10,120, "Dragoons: %i (%i)", getNoUnitsByType(Self,UnitTypes::Protoss_Dragoon), UnitTypes::Protoss_Dragoon.destroyScore() );

	//Broodwar->drawTextScreen(10,150, "Enemy GroundAttStr: %i", getGroundAttackStr(Enemy));
	//Broodwar->drawTextScreen(10,165, "Self GroundAttStr: %i", getGroundAttackStr(Self));

	//Broodwar->drawTextScreen(10,60, "Total enemy ground force: %i (%i)", enemyForces.totGround, playerForces.totGround);
	//Broodwar->drawTextScreen(10,75, "Total enemy air force: %i (%i)", enemyForces.totAir, playerForces.totAir);
	//Broodwar->drawTextScreen(10,90, "Total enemy buildings: %i (%i)", enemyForces.totBuildings, playerForces.totBuildings);
	//Broodwar->drawTextScreen(10,105, "Enemy air strength: %i (%i)", enemyForces.airAttackStr, playerForces.airAttackStr);
	//Broodwar->drawTextScreen(10,120, "Enemy ground strength: %i (%i)", enemyForces.groundAttackStr, playerForces.groundAttackStr);
	//Broodwar->drawTextScreen(10,135, "Enemy air defense: %i (%i)", enemyForces.airDefenseStr, playerForces.airDefenseStr);
	//Broodwar->drawTextScreen(10,150, "Enemy ground defense: %i (%i)", enemyForces.groundDefenseStr, playerForces.groundDefenseStr);

}

int ExplorationManager::getNoDetectors( UserType p_Type )
{
	int count = 0;
	PlayerForce f;

	if(p_Type == Self)
		f = playerForces;
	else
		f = enemyForces;


	if( f.race == Races::Terran.getID() )
	{
		std::map<int,int>::const_iterator i;

		i = enemyForces.airForce.find( UnitTypes::Terran_Science_Vessel.getID() );
		count += i->second;

		i = enemyForces.groundForce.find( UnitTypes::Terran_Vulture_Spider_Mine.getID() );
		count += i->second;

		i = enemyForces.buildings.find( UnitTypes::Terran_Comsat_Station.getID() );
		count += i->second;

		i = enemyForces.buildings.find( UnitTypes::Terran_Missile_Turret.getID() );
		count += i->second;

	}

	else if( f.race == Races::Protoss.getID() )
	{
		std::map<int,int>::iterator i;

		i = enemyForces.airForce.find( UnitTypes::Protoss_Observer.getID() );
		count += i->second;

		i = enemyForces.buildings.find( UnitTypes::Protoss_Photon_Cannon.getID() );
		count += i->second;

	}
	else if( f.race == Races::Zerg.getID() )
	{
		std::map<int,int>::iterator i;

		i = enemyForces.airForce.find( UnitTypes::Zerg_Overlord.getID() );
		count += i->second;

		i = enemyForces.buildings.find( UnitTypes::Zerg_Spore_Colony.getID() );
		count += i->second;
	}

	return count;
}

int ExplorationManager::getNoCommandCenters( UserType p_Type )
{
	int count = 0;
	PlayerForce f;

	if(p_Type == Self)
		f = playerForces;
	else
		f = enemyForces;

	if( f.race == Races::Terran.getID() )
	{
		std::map<int,int>::iterator i;

		i = enemyForces.buildings.find( UnitTypes::Terran_Command_Center.getID() );
		count += i->second;
	}

	else if( f.race == Races::Protoss.getID() )
	{
		std::map<int,int>::iterator i;

		i = enemyForces.buildings.find( UnitTypes::Protoss_Nexus.getID() );
		count += i->second;
	}
	else if( f.race == Races::Zerg.getID() )
	{
		std::map<int,int>::iterator i;

		i = enemyForces.airForce.find( UnitTypes::Zerg_Hatchery.getID() );
		count += i->second;

		i = enemyForces.buildings.find( UnitTypes::Zerg_Lair.getID() );
		count += i->second;
	}

	return count;
}

int ExplorationManager::getNoGroundDefense( UserType p_Type )
{
	int count = 0;
	PlayerForce f;

	if(p_Type == Self)
		f = playerForces;
	else
		f = enemyForces;


	if( f.race == Races::Terran.getID() )
	{
		std::map<int,int>::iterator i;

		i = enemyForces.buildings.find( UnitTypes::Terran_Bunker.getID() );
		count += i->second;
	}

	else if( f.race == Races::Protoss.getID() )
	{
		std::map<int,int>::iterator i;

		i = enemyForces.airForce.find( UnitTypes::Protoss_Photon_Cannon.getID() );
		count += i->second;
	}
	else if( f.race == Races::Zerg.getID() )
	{
		std::map<int,int>::iterator i;

		i = enemyForces.airForce.find( UnitTypes::Zerg_Sunken_Colony.getID() );
		count += i->second;
	}

	return count;
}
int ExplorationManager::getNoAirDefense( UserType p_Type )
{
	int count = 0;

	PlayerForce f;

	if(p_Type == Self)
		f = playerForces;
	else
		f = enemyForces;


	if( f.race == Races::Terran.getID() )
	{
		std::map<int,int>::iterator i;

		i = enemyForces.buildings.find( UnitTypes::Terran_Missile_Turret.getID() );
		count += i->second;
	}

	else if( f.race == Races::Protoss.getID() )
	{
		std::map<int,int>::iterator i;

		i = enemyForces.airForce.find( UnitTypes::Protoss_Photon_Cannon.getID() );
		count += i->second;
	}
	else if( f.race == Races::Zerg.getID() )
	{
		std::map<int,int>::iterator i;

		i = enemyForces.airForce.find( UnitTypes::Zerg_Spore_Colony.getID() );
		count += i->second;
	}

	return count;
}

int ExplorationManager::getNoGroundForce( UserType p_Type )
{
	PlayerForce f;

	if(p_Type == Self)
		f = playerForces;
	else
		f = enemyForces;

	return f.totGround;
}
int ExplorationManager::getNoAirForce( UserType p_Type )
{
	PlayerForce f;

	if(p_Type == Self)
		f = playerForces;
	else
		f = enemyForces;

	return f.totAir;
}
int ExplorationManager::getNoBuildings( UserType p_Type )
{
	PlayerForce f;

	if(p_Type == Self)
		f = playerForces;
	else
		f = enemyForces;


	return f.totBuildings;
}

int ExplorationManager::getGroundDefenseStr( UserType p_Type )
{
	PlayerForce f;

	if(p_Type == Self)
		f = playerForces;
	else
		f = enemyForces;

	return f.groundAttackStr + f.groundDefenseStr + ( (Broodwar->elapsedTime() / 60) * 150) ;
}
int ExplorationManager::getGroundAttackStr( UserType p_Type )
{
	PlayerForce f;

	if(p_Type == UserType::Self)
		f = playerForces;
	else
		f = enemyForces;

	return f.groundAttackStr + ( (Broodwar->elapsedTime() / 60) * 150) ;
}
int ExplorationManager::getAirDefenseStr( UserType p_Type )
{
	PlayerForce f;

	if(p_Type == Self)
		f = playerForces;
	else
		f = enemyForces;

	return f.airAttackStr + f.airDefenseStr + ( (Broodwar->elapsedTime() / 60) * 150) ;
}
int ExplorationManager::getAirAttackStr( UserType p_Type )
{
	PlayerForce f;

	if(p_Type == Self)
		f = playerForces;
	else
		f = enemyForces;


	return f.airAttackStr + ( (Broodwar->elapsedTime() / 60) * 150) ;
}

int ExplorationManager::getNoUnitsByType( UserType p_UserType, UnitType p_UnitType)
{
	


	PlayerForce f;

	if(p_UserType == Self)
		f = playerForces;
	else
		f = enemyForces;

	if(p_UnitType.getRace().getID() != f.race)
		return -1;


	std::map<int,int>::iterator i;

	if( p_UnitType.isBuilding() )
	{
		i = f.buildings.find( p_UnitType.getID() );
		return i->second;

	}
	else if( p_UnitType.isFlyer() )
	{
		i = f.airForce.find( p_UnitType.getID() );
		return i->second;

	}
	else
	{
		i = f.groundForce.find( p_UnitType.getID() );
		return i->second;
	}

	return 0;
}

void ExplorationManager::resetEnemyForce(void)
{
	for(std::map<int,int>::iterator i = enemyForces.airForce.begin(); i != enemyForces.airForce.end(); i++)
		i->second = 0;
	for(std::map<int,int>::iterator i = enemyForces.buildings.begin(); i != enemyForces.buildings.end(); i++)
		i->second = 0;
	for(std::map<int,int>::iterator i = enemyForces.groundForce.begin(); i != enemyForces.groundForce.end(); i++)
		i->second = 0;

	enemyForces.totGround = 0;
	enemyForces.totAir = 0;
	enemyForces.totBuildings = 0;
	enemyForces.airAttackStr = 0;
	enemyForces.groundAttackStr = 0;
	enemyForces.airDefenseStr = 0;
	enemyForces.groundDefenseStr = 0;
}
void ExplorationManager::resetPlayerForce(void)
{
	for(std::map<int,int>::iterator i = playerForces.airForce.begin(); i != playerForces.airForce.end(); i++)
		i->second = 0;
	for(std::map<int,int>::iterator i = playerForces.buildings.begin(); i != playerForces.buildings.end(); i++)
		i->second = 0;
	for(std::map<int,int>::iterator i = playerForces.groundForce.begin(); i != playerForces.groundForce.end(); i++)
		i->second = 0;

	playerForces.totGround = 0;
	playerForces.totAir = 0;
	playerForces.totBuildings = 0;
	playerForces.airAttackStr = 0;
	playerForces.groundAttackStr = 0;
	playerForces.airDefenseStr = 0;
	playerForces.groundDefenseStr = 0;
}

void ExplorationManager::calcOwnForceData()
{
	resetPlayerForce();

	vector<BaseAgent*> agents = AgentManager::getInstance()->getAgents();
	for (int i = 0; i < (int)agents.size(); i++)
	{
		if ( agents.at(i)->isAlive())
		{
			UnitType type = agents.at(i)->getUnitType();
			if(!type.isBuilding())
				addPlayerUnit(type.getID());
			else
				addPlayerBuilding(type.getID());


			if(type.isWorker())
				continue;

			if(!type.isBuilding())
			{
				if (UnitAgent::getAirRange(type) >= 0)
				{
					playerForces.airAttackStr += type.destroyScore();
				}
				if (UnitAgent::getGroundRange(type) >= 0)
				{
					playerForces.groundAttackStr += type.destroyScore();
				}

			}
			else
			{
				if (UnitAgent::getAirRange(type) >= 0)
				{
					playerForces.airDefenseStr += type.destroyScore();
				}
				if (UnitAgent::getGroundRange(type) >= 0)
				{
					playerForces.groundDefenseStr += type.destroyScore();
				}
			}
		}

	}

	playerForces.airAttackStr *= 0.8;
	playerForces.airDefenseStr *= 0.8;
	playerForces.groundAttackStr *= 0.8;
	playerForces.groundDefenseStr *= 0.8;


}
void ExplorationManager::calcEnemyForceData()
{
	resetEnemyForce();

	for(int i = 0; i < (int)spottedUnits.size(); i++)
	{
		UnitType type = spottedUnits[i]->getType();
		addEnemyUnit(type.getID());

		if(type.isWorker())
			continue;

		if (UnitAgent::getAirRange(type) >= 0)
			enemyForces.airAttackStr += type.destroyScore();
		if (UnitAgent::getGroundRange(type) >= 0)
			enemyForces.groundAttackStr += type.destroyScore();
		else if( UnitTypes::Terran_Medic.getID() == type.getID())
			enemyForces.groundAttackStr += type.destroyScore();

	}

	for(int i = 0; i < (int)spottedBuildings.size(); i++)
	{
		UnitType type = spottedBuildings[i]->getType();
		addEnemyBuilding(type.getID());

		if (UnitAgent::getAirRange(type) >= 0)
			enemyForces.airDefenseStr += type.destroyScore();
		if (UnitAgent::getGroundRange(type) >= 0)
			enemyForces.groundDefenseStr += type.destroyScore(); 
	}

}

void ExplorationManager::addEnemyUnit(int id)
{
	std::map<int,int>::iterator i = enemyForces.groundForce.find(id);

	if(i != enemyForces.groundForce.end()) // unit found
	{
		enemyForces.totGround++;
		i->second++;
	}
	else
	{
		i = enemyForces.airForce.find(id);
		i->second++;
		enemyForces.totAir++;
	}

}
void ExplorationManager::addEnemyBuilding(int id)
{
	std::map<int,int>::iterator i = enemyForces.buildings.find(id);

	if(i != enemyForces.buildings.end()) // unit found
	{
		i->second++;
		enemyForces.totBuildings++;
	}
}

void ExplorationManager::addPlayerUnit(int id)
{
	std::map<int,int>::iterator i = playerForces.groundForce.find(id);

	if(i != playerForces.groundForce.end()) // unit found
	{
		i->second++;
		playerForces.totGround++;
	}
	else
	{
		i = playerForces.airForce.find(id);
		i->second++;
		playerForces.totAir++;
	}

}
void ExplorationManager::addPlayerBuilding(int id)
{
	std::map<int,int>::iterator i = playerForces.buildings.find(id);

	if(i != playerForces.buildings.end()) // unit found
	{
		i->second++;
		playerForces.totBuildings++;
	}
}


void ExplorationManager::printInfo()
{
	//Uncomment this if you want to draw a mark at detected enemy buildings.
	/*for (int i = 0; i < (int)spottedBuildings.size(); i++)
	{
	if (spottedBuildings.at(i)->isActive())
	{
	int x1 = spottedBuildings.at(i)->getTilePosition().x() * 32;
	int y1 = spottedBuildings.at(i)->getTilePosition().y() * 32;
	int x2 = x1 + 32;
	int y2 = y1 + 32;

	Broodwar->drawBoxMap(x1,y1,x2,y2,Colors::Blue,true);
	}
	}*/

	//Draw a circle around detectors
}

void ExplorationManager::addSpottedUnit(Unit* unit)
{
	if (unit->getType().isBuilding())
	{

		//Check if we already have seen this building
		bool found = false;
		for (int i = 0; i < (int)spottedBuildings.size(); i++)
		{
			if (spottedBuildings.at(i)->getUnitID() == unit->getID())
			{
				found = true;
				break;
			}
		}

		if (!found)
		{
			spottedBuildings.push_back(new SpottedObject(unit));
		}
	}
	else
	{
		bool found = false;
		for (int i = 0; i < (int)spottedUnits.size(); i++)
		{
			if (spottedUnits.at(i)->getUnitID() == unit->getID())
			{
				if(spottedUnits.at(i)->getType().getID() != unit->getType().getID())
				{
					spottedUnits.at(i) = new SpottedObject(unit);
				}


				found = true;
				break;
			}
		}

		if (!found)
			spottedUnits.push_back(new SpottedObject(unit));
	}
}

void ExplorationManager::unitDestroyed(Unit* unit)
{
	TilePosition uPos = unit->getTilePosition();
	if (unit->getType().isBuilding())
	{
		bool removed = false;
		for (int i = 0; i < (int)spottedBuildings.size(); i++)
		{
			TilePosition sPos = spottedBuildings.at(i)->getTilePosition();
			if (uPos.x() == sPos.x() && uPos.y() == sPos.y())
			{
				spottedBuildings.erase(spottedBuildings.begin()+i);
				removed = true;
			}
		}

		if (!removed)
		{
			//Broodwar->printf("[EM]: Building %s at (%d,%d) was not removed from EM!!!", unit->getType().getName().c_str(), uPos.x(), uPos.y());
		}
	}
	else
	{
		for (int i = 0; i < (int)spottedUnits.size(); i++)
		{
			if (spottedUnits.at(i)->getUnitID() == unit->getID())
			{
				spottedUnits.erase(spottedUnits.begin()+i);
				//Broodwar->printf("[EM]: Remove %s at (%d,%d)", unit->getType().getName().c_str(), uPos.x(), uPos.y());
			}
		}
	}
}

void ExplorationManager::cleanup()
{
	for (int i = 0; i < (int)spottedBuildings.size(); i++)
	{
		if (spottedBuildings.at(i)->isActive())
		{
			if (Broodwar->isVisible(spottedBuildings.at(i)->getTilePosition()))
			{
				int id = spottedBuildings.at(i)->getUnitID();
				bool found = false;
				for(set<Unit*>::const_iterator it=Broodwar->enemy()->getUnits().begin();it!=Broodwar->enemy()->getUnits().end();it++)
				{
					if ((*it)->exists())
					{
						if ((*it)->getID() == id)
						{
							found = true;
							break;
						}
					}
				}
				if (!found)
				{
					spottedBuildings.at(i)->setInactive();
				}
			}
		}
	}
}

int ExplorationManager::spottedBuildingsWithinRange(TilePosition pos, int range)
{
	cleanup();

	int eCnt = 0;
	for (int i = 0; i < (int)spottedBuildings.size(); i++)
	{
		if (spottedBuildings.at(i)->isActive())
		{
			if (pos.getDistance(spottedBuildings.at(i)->getTilePosition()) <= range)
			{
				eCnt++;
			}
		}
	}

	return eCnt;
}

TilePosition ExplorationManager::getClosestSpottedBuilding(TilePosition start)
{
	cleanup();

	TilePosition pos = TilePosition(-1, -1);
	double bestDist = 100000;

	for (int i = 0; i < (int)spottedBuildings.size(); i++)
	{
		if (spottedBuildings.at(i)->isActive())
		{
			double cDist = start.getDistance(spottedBuildings.at(i)->getTilePosition());
			if (cDist < bestDist)
			{
				bestDist = cDist;
				pos = spottedBuildings.at(i)->getTilePosition();
			}
		}
	}

	return pos;
	
}

const vector<SpottedObject*>& ExplorationManager::getSpottedBuildings()
{
	cleanup();
	return spottedBuildings;
}

bool ExplorationManager::buildingsSpotted()
{
	if (spottedBuildings.size() > 0)
	{
		return true;
	}
	return false;
}

bool ExplorationManager::canReach(TilePosition a, TilePosition b)
{
	int w = Broodwar->mapWidth();
	int h = Broodwar->mapHeight();
	if (a.x() < 0 || a.x() >= w || a.y() < 0 || a.y() >= h)
	{
		return false;
	}
	if (b.x() < 0 || b.x() >= w || b.y() < 0 || b.y() >= h)
	{
		return false;
	}
	bool ok = true;

	ok = a.hasPath(b);

	return ok;
}

bool ExplorationManager::canReach(BaseAgent* agent, TilePosition b)
{
	return agent->getUnit()->hasPath(Position(b));
}

bool ExplorationManager::enemyIsProtoss()
{
	for(set<Player*>::const_iterator i=Broodwar->getPlayers().begin();i!=Broodwar->getPlayers().end();i++)
	{
		if ((*i)->isEnemy(Broodwar->self()))
		{
			if ((*i)->getRace().getID() == Races::Protoss.getID())
			{
				return true;
			}
		}
	}
	return false;
}

bool ExplorationManager::enemyIsZerg()
{
	for(set<Player*>::const_iterator i=Broodwar->getPlayers().begin();i!=Broodwar->getPlayers().end();i++)
	{
		if ((*i)->isEnemy(Broodwar->self()))
		{
			if ((*i)->getRace().getID() == Races::Zerg.getID())
			{
				return true;
			}
		}
	}
	return false;
}

bool ExplorationManager::enemyIsTerran()
{
	for(set<Player*>::const_iterator i=Broodwar->getPlayers().begin();i!=Broodwar->getPlayers().end();i++)
	{
		if ((*i)->isEnemy(Broodwar->self()))
		{
			if ((*i)->getRace().getID() == Races::Terran.getID())
			{
				return true;
			}
		}
	}
	return false;
}

bool ExplorationManager::enemyIsUnknown()
{
	if (!enemyIsTerran() && !enemyIsProtoss() && !enemyIsZerg())
	{
		return true;
	}
	return false;
}

TilePosition ExplorationManager::scanForVulnerableBase()
{
	TilePosition spot = TilePosition(-1, -1);
	for (int i = 0; i < (int)spottedBuildings.size(); i++)
	{
		if (spottedBuildings.at(i)->isActive())
		{
			SpottedObject* obj = spottedBuildings.at(i);
			if (obj->getType().isResourceDepot())
			{
				if (!isDetectorCovering(obj->getTilePosition()))
				{
					//Broodwar->printf("Found probable vulnerable base at (%d,%d)", obj->getTilePosition().x(), obj->getTilePosition().y());
					spot = obj->getTilePosition();
				}
			}
		}
	}

	if (spot.x() < 0)
	{
		//Broodwar->printf("Scan: No vulnerable base found");
	}

	return spot;
}

bool ExplorationManager::isDetectorCovering(TilePosition pos)
{
	return isDetectorCovering(Position(pos));
}

bool ExplorationManager::isDetectorCovering(Position pos)
{
	for (int i = 0; i < (int)spottedBuildings.size(); i++)
	{
		if (spottedBuildings.at(i)->isActive())
		{
			SpottedObject* obj = spottedBuildings.at(i);
			if (obj->getType().isDetector())
			{
				double dist = obj->getPosition().getDistance(pos);
				if (dist <= obj->getType().sightRange())
				{
					return true;
				}
			}
		}
	}
	return false;
}