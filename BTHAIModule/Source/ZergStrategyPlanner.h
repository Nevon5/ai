#ifndef ZERGSTRATEGYPLANNER_H
#define ZERGSTRATEGYPLANNER_H

#include "StrategyPlanner.h"
#include "Commander.h"

using namespace BWAPI;
using namespace BWTA;
using namespace std;



class ZergStrategyPlanner : public StrategyPlanner
{
private:
	void counter( std::vector<Squad*>& p_Squad );
public:
	ZergStrategyPlanner( vector<BuildplanEntry>* p_Calcbuilder, int* p_noOfWorkers );
	~ZergStrategyPlanner(void);

	bool OpeningStrategy(int p_Supply, std::vector<Squad*>& p_Squad);
	bool EarlyStrategy(int p_Supply, std::vector<Squad*>& p_Squad);
	bool MiddleStrategy(int p_Supply, std::vector<Squad*>& p_Squad);
	bool LateStrategy(int p_Supply, std::vector<Squad*>& p_Squad);
	void AllDoneStrategy(int p_Supply, std::vector<Squad*>& p_Squad);
};

#endif