#include "DragoonAgent.h"
#include "NavigationAgent.h"
#include "TargetingAgent.h"

DragoonAgent::DragoonAgent(Unit* mUnit)
{
	unit = mUnit;
	type = unit->getType();
	unitID = unit->getID();
	agentType = "DragoonAgent";
	
	goal = TilePosition(-1, -1);
}

void DragoonAgent::computeActions()
{
	NavigationAgent::getInstance()->computeMove(this, goal, false);

	Unit* attack = unit->getTarget();
	if ( (unit->getAirWeaponCooldown( ) == 0 ||  unit->getGroundWeaponCooldown( ) == 0)
		&& attack != NULL)
	{
		if ( canAttack( unit->getType( ), attack->getType( ) ) )
		{
			unit->attack( attack );
			unit->rightClick(attack);
		}
	}
	else if ( attack != NULL )
	{
		//Broodwar->printf("ARG!");
		Position move = unit->getPosition( ) - attack->getPosition( );
		goal = TilePosition( move );
		unit->move( move );
	}

}
