#include "TerranStrategyPlanner.h"

TerranStrategyPlanner::TerranStrategyPlanner( vector<BuildplanEntry>* p_Calcbuilder, int* p_noOfWorkers  )
	: StrategyPlanner( p_Calcbuilder, p_noOfWorkers )
{

		m_CalcBuildPlan->push_back(BuildplanEntry(UnitTypes::Protoss_Pylon, 0));
		m_CalcBuildPlan->push_back(BuildplanEntry(UnitTypes::Protoss_Gateway, 0)); 
		m_CalcBuildPlan->push_back(BuildplanEntry(UnitTypes::Protoss_Gateway, 0)); 
		m_CalcBuildPlan->push_back(BuildplanEntry(UnitTypes::Protoss_Pylon, 0));
		m_CalcBuildPlan->push_back(BuildplanEntry(UnitTypes::Protoss_Assimilator, 12));
		m_CalcBuildPlan->push_back(BuildplanEntry(UnitTypes::Protoss_Cybernetics_Core, 0));
		m_CalcBuildPlan->push_back(BuildplanEntry(UnitTypes::Protoss_Pylon, 0));

		buildforge( );

};

TerranStrategyPlanner::~TerranStrategyPlanner( void )
{
};

void TerranStrategyPlanner::counter( std::vector<Squad*>& p_Squad )
{
	ExplorationManager *temp = temp->getInstance(  );
	vector<UnitSetup> a = counterSquad->getSetup();

	int units = temp->getNoUnitsByType( UserType::Enemy, UnitTypes::Terran_Marine ) * 2;
	if ( units > 0 ) // counter for marines!
	{
		
		int noZ = getNoUnit( a, UnitTypes::Protoss_Zealot );
		int noHT = getNoUnit( a, UnitTypes::Protoss_High_Templar );

		if ( noZ < units/2 )
		{
			counterSquad->addSetup( UnitTypes::Protoss_Zealot, units/2 - noZ);
		}
		if ( noHT < units/5 )
		{
			buildcyberneticsCore( );
			buildtemplarArchives( );

			if ( !haveStorm )
			{
				m_CalcBuildPlan->push_back(BuildplanEntry(TechTypes::Psionic_Storm, 0));
				haveStorm = true;
			}
			counterSquad->addSetup( UnitTypes::Protoss_High_Templar, units - noHT);
		}
	}

	units = temp->getNoUnitsByType( UserType::Enemy, UnitTypes::Terran_Firebat )*2;
	if ( units > 0 ) //counter for firebat
	{
		buildcyberneticsCore( );
		
		int noD = getNoUnit( a, UnitTypes::Protoss_Dragoon );
		int noZ = getNoUnit( a, UnitTypes::Protoss_Zealot );


		if ( noZ < units/2 )
		{
			counterSquad->addSetup( UnitTypes::Protoss_Zealot, units/2 - noZ);
		}
		if ( noD < units/2 )
		{
			counterSquad->addSetup( UnitTypes::Protoss_Dragoon, units/2 - noD);
		}
	}

	units = temp->getNoUnitsByType( UserType::Enemy, UnitTypes::Terran_Ghost )*2;

	if ( units > 0 || (Broodwar->self()->minerals( ) > 100 && Broodwar->self()->gas( ) > 100 ) ) // counter for ghost
	{
		buildobservatory( );

		if ( !canbuildObs )
		{
			m_Squad = new ScoutSquad( squadID, "obsscout", 1, Squad::AIR );
			m_Squad->addSetup( UnitTypes::Protoss_Observer, 1 );
			m_Squad->setBuildup( false );
			m_Squad->setRequired( true );
			p_Squad.push_back( m_Squad );
			squadID++;
			canbuildObs = true;
		}
		if ( units != 0 )
		{
			int noD = getNoUnit( a, UnitTypes::Protoss_Dragoon );
			int noO = getNoUnit( a, UnitTypes::Protoss_Observer );

			if ( noO == 0 )
			{
				counterSquad->addSetup( UnitTypes::Protoss_Observer, 1);
			}
			if ( noD < units*2 )
			{
				counterSquad->addSetup( UnitTypes::Protoss_Dragoon, units*2 - noD);
			}
		}
	}

	units = temp->getNoUnitsByType( UserType::Enemy, UnitTypes::Terran_Medic )*2;
	if ( units > 0 ) // coutner for medic
	{
		buildtemplarArchives( );

		int noDT = getNoUnit( a, UnitTypes::Protoss_Dark_Templar );

		if ( noDT < units/2 )
		{
			counterSquad->addSetup( UnitTypes::Protoss_Dark_Templar, units/2 - noDT);
		}
	}

	units = temp->getNoUnitsByType( UserType::Enemy, UnitTypes::Terran_Vulture )*2;
	if ( units > 0 )// counter for Vulture
	{
		buildcyberneticsCore( );

		int noD = getNoUnit( a, UnitTypes::Protoss_Dragoon );

		if ( noD < units*2 )
		{
			counterSquad->addSetup( UnitTypes::Protoss_Dragoon, units*2 - noD);
		}
	}

	units = temp->getNoUnitsByType( UserType::Enemy, UnitTypes::Terran_Siege_Tank_Tank_Mode )*2
		+ temp->getNoUnitsByType( UserType::Enemy, UnitTypes::Terran_Siege_Tank_Siege_Mode )*2;
	if ( units > 0 ) // counter for siege tanks
	{
		buildtemplarArchives( );

		if ( !haveStorm )
		{
			m_CalcBuildPlan->push_back(BuildplanEntry(TechTypes::Psionic_Storm, 0));
			haveStorm = true;
		}

		int noHT = getNoUnit( a, UnitTypes::Protoss_High_Templar );
		int noZ = getNoUnit( a, UnitTypes::Protoss_Zealot );

		if ( noHT < units/2 )
		{
			counterSquad->addSetup( UnitTypes::Protoss_High_Templar, units/2 - noHT);
		}
		if ( noZ < units*3 )
		{
			counterSquad->addSetup( UnitTypes::Protoss_Zealot, units*3 - noZ);
		}
	}

	units = temp->getNoUnitsByType( UserType::Enemy, UnitTypes::Terran_Goliath )*2;
	if ( units > 0 ) // coutner for Goliath
	{
		int noZ = getNoUnit( a, UnitTypes::Protoss_Zealot );

		if ( noZ < units*2 )
		{
			counterSquad->addSetup( UnitTypes::Protoss_Zealot, units*2 - noZ);
		}
	}

	units = temp->getNoUnitsByType( UserType::Enemy, UnitTypes::Terran_Wraith )*2;
	if ( units > 0 ) // counter for Wraith
	{
		buildstargate( );
		buildobservatory( );

		int noS = getNoUnit( a, UnitTypes::Protoss_Scout );
		int noO = getNoUnit( a, UnitTypes::Protoss_Observer );

		if ( noS < units )
		{
			counterSquad->addSetup( UnitTypes::Protoss_Scout, units - noS);
		}
		if ( noO == 0 )
		{
			counterSquad->addSetup( UnitTypes::Protoss_Observer, 1);
		}
	}

	units = temp->getNoUnitsByType( UserType::Enemy, UnitTypes::Terran_Dropship )*2;
	if ( units > 0 ) // counter for Dropship
	{
		buildstargate( );

		int noS = getNoUnit( a, UnitTypes::Protoss_Scout );

		if ( noS < units )
		{
			counterSquad->addSetup( UnitTypes::Protoss_Scout, units - noS);
		}
	}

	units = temp->getNoUnitsByType( UserType::Enemy, UnitTypes::Terran_Battlecruiser )*2;
	if ( units > 0 ) // counter for Battlecruiser
	{
		buildstargate( );

		int noS = getNoUnit( a, UnitTypes::Protoss_Scout );

		if ( noS < units*3 )
		{
			counterSquad->addSetup( UnitTypes::Protoss_Scout, units*3 - noS);
		}
	}
	units = temp->getNoUnitsByType( UserType::Enemy, UnitTypes::Terran_Science_Vessel )*2;
	if ( units > 0 ) // counter for Science Vessel
	{
		buildstargate( );

		int noS = getNoUnit( a, UnitTypes::Protoss_Scout );

		if ( noS < units )
		{
			counterSquad->addSetup( UnitTypes::Protoss_Scout, units - noS);
		}
	}
	units = temp->getNoUnitsByType( UserType::Enemy, UnitTypes::Terran_Valkyrie )*2;
	if ( units > 0 ) // counter for Valkyrie
	{
		buildstargate( );

		int noS = getNoUnit( a, UnitTypes::Protoss_Scout );

		if ( noS < units )
		{
			counterSquad->addSetup( UnitTypes::Protoss_Scout, units - noS);
		}
	}

	if ( Broodwar->self()->minerals( ) > 1000 ) // have some spare minerals?
	{
		if ( !expandStar ) // expand with Stargates
		{
			m_CalcBuildPlan->push_back(BuildplanEntry(UnitTypes::Protoss_Stargate, 0));
			m_CalcBuildPlan->push_back(BuildplanEntry(UnitTypes::Protoss_Stargate, 0));
			expandStar = true;
		}
		else if ( !expandGate ) // or with Gateways
		{
			m_CalcBuildPlan->push_back(BuildplanEntry(UnitTypes::Protoss_Gateway, 0));
			m_CalcBuildPlan->push_back(BuildplanEntry(UnitTypes::Protoss_Gateway, 0));
			expandGate = true;
		}

	}

	if ( gwlvl == 0 )// This will do some uppgrades if we have some extra resources
	{
		if ( forge && Broodwar->self()->minerals() > 200 && Broodwar->self()->gas() > 200  )
		{
			m_CalcBuildPlan->push_back(BuildplanEntry(UpgradeTypes::Protoss_Ground_Weapons, 0));
			gwlvl++;
		}
	}
	else if ( galvl == 0 )// This will do some uppgrades if we have some extra resources
	{
		if ( forge && Broodwar->self()->minerals( ) > 200 && Broodwar->self()->gas( ) > 200 )
		{
			m_CalcBuildPlan->push_back(BuildplanEntry(UpgradeTypes::Protoss_Ground_Armor, 0));
			galvl++;
		}
	}

	if ( Broodwar->self()->minerals() > 200 )
	{
		if ( counterSquad->isFull( ) && counterSquad->size( ) > 10 )
		{
			buildcyberneticsCore( );
			buildstargate( );
			buildtemplarArchives( );
			

			buildarbiterTribunal( );

			// Learns some new techs!
			m_CalcBuildPlan->push_back(BuildplanEntry(TechTypes::Stasis_Field, 0));
			if ( !haveStorm )
			{
				m_CalcBuildPlan->push_back(BuildplanEntry(TechTypes::Psionic_Storm, 0));
				haveStorm = true;
			}
			// give the countersquade some boost if needed.
			int noA = getNoUnit(a, UnitTypes::Protoss_Arbiter );

			if ( noA == 0 )
			{
				counterSquad->addSetup( UnitTypes::Protoss_Arbiter, 1);
			}
		}
	}

}

bool TerranStrategyPlanner::OpeningStrategy( int p_Supply, std::vector<Squad*>& p_Squad )
{

	if(p_Supply >= 9 && squadID == 0)
	{
		m_Squad = new Squad(squadID,Squad::DEFENSIVE,"1th",1);
		m_Squad->addSetup( UnitTypes::Protoss_Zealot, 10 );
		m_Squad->addSetup( UnitTypes::Protoss_Dragoon, 5 );
		m_Squad->setBuildup( false );
		m_Squad->setRequired( true );

		p_Squad.push_back( m_Squad );
		squadID++;

		WorkerAgent* ba;
		ba = (WorkerAgent*)AgentManager::getInstance()->findClosestFreeWorker(Broodwar->self()->getStartLocation());
		ba->clearGoal();
		ba->setState(7);
		m_Squad = new ScoutSquad(squadID,"First Scout",1,Squad::GROUND);
		m_Squad->addSetup(UnitTypes::Protoss_Probe,1);
		m_Squad->addMember(ba);
		m_Squad->setBuildup(true);
		m_Squad->setRequired(false);
		m_Squad->setPriority(1001);

		p_Squad.push_back(m_Squad);
		squadID++;


		counterSquad = new Squad(squadID,Squad::OFFENSIVE,"counter",4);
		counterSquad->addSetup(UnitTypes::Protoss_Zealot, 1);
		counterSquad->setBuildup(true);
		counterSquad->setRequired(true);

		p_Squad.push_back(counterSquad);
		squadID++;
		return true; // Done
	}
	return false;
}
bool TerranStrategyPlanner::EarlyStrategy( int p_Supply, std::vector<Squad*>& p_Squad )
{
	counter( p_Squad );
	if (p_Squad[0]->isFull( ) )
	{
		p_Squad[0]->setActivePriority( 1000000 );
		p_Squad[0]->setPriority( 1000000 );
	}
	

	PhotonCannons();

	if ( (counterSquad->size( ) > 50 && !ta) || (Broodwar->elapsedTime() > 900 && !ta) )
	{
		ta = true;
		buildforge();
		for ( ; gwlvl < 3; gwlvl++ )
		{
			m_CalcBuildPlan->push_back(BuildplanEntry(UpgradeTypes::Protoss_Ground_Weapons, 0));
		}
		for ( ; galvl < 3; galvl++ )
		{
			m_CalcBuildPlan->push_back(BuildplanEntry(UpgradeTypes::Protoss_Ground_Armor, 0));
		}
		m_Squad = new RushSquad(squadID,Squad::RUSH,"end",1);
		m_Squad->addSetup( UnitTypes::Protoss_Zealot, 10 );
		m_Squad->addSetup( UnitTypes::Protoss_Dragoon, 5 );
		m_Squad->setBuildup( true );
		m_Squad->setRequired( true );
		p_Squad.push_back(m_Squad);

		squadID++;
		m_Squad = new RushSquad(squadID,Squad::RUSH,"end it",2);
		m_Squad->addSetup( UnitTypes::Protoss_Zealot, 15 );
		m_Squad->addSetup( UnitTypes::Protoss_Dragoon, 10 );
		m_Squad->setBuildup( true );
		m_Squad->setRequired( true );
		p_Squad.push_back(m_Squad);

		squadID++;
		m_Squad = new RushSquad(squadID, Squad::RUSH,"end it now!",3);
		m_Squad->addSetup( UnitTypes::Protoss_Zealot, 15 );
		m_Squad->addSetup( UnitTypes::Protoss_Dragoon, 15 );
		m_Squad->setBuildup( true );
		m_Squad->setRequired( true );
		p_Squad.push_back(m_Squad);
		squadID++;

	}

	if( Broodwar->elapsedTime() >= 1500 && !forceAttack)
	{
		forceAttack = true;
		Commander::getInstance()->forceAttack();
	}

	return false;
}

bool TerranStrategyPlanner::MiddleStrategy( int p_Supply, std::vector<Squad*>& p_Squad )
{
	return false;
}

bool TerranStrategyPlanner::LateStrategy( int p_Supply, std::vector<Squad*>& p_Squad )
{
	return false;
}

void TerranStrategyPlanner::AllDoneStrategy( int p_Supply, std::vector<Squad*>& p_Squad )
{
}